﻿'use strict';
marombaApp.factory('sessionService', ['localStorageService', '$location', '$rootScope', 
     function (localStorageService, $location, $rootScope) {

         var dataFactory = {};

         dataFactory.validarSession = function () {
             var usuario = localStorageService.obter('usuario');

             if (usuario != undefined) {
                 $rootScope.usuario = usuario;
             }

             if ($rootScope.usuario.Id <= 0) {
                 $location.url('/login');
             }
         };

         dataFactory.usuarioLogado = function () {
             var usuario = localStorageService.obter('usuario');
             if (usuario != undefined) {
                 $rootScope.usuario = usuario;
                 $location.url('/inicio');
                 return true;
             } else {
                 return false;
             }
         };

         return dataFactory;
     }]);