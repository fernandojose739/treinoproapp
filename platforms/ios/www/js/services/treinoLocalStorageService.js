﻿'use strict';
marombaApp.factory('treinoLocalStorageService', ['configApp', '$http',
     function (configApp, $http) {

         var dataFactory = {};
         var db = '';
         var shortName = 'WebSqlDB';
         var version = '1.0';
         var displayName = 'WebSqlDB';
         var maxSize = 1000000;

         dataFactory.salvar = function (treino) {
             if (!inicializaBandoDeDados()) {
                 return false;
             }

             //--- insert
             db.transaction(function (transaction) {
                 transaction.executeSql('INSERT INTO Treino(UsuarioId, Site, Login, Senha) VALUES (?,?,?,?)', [usuarioId, site, login, senha], salvarSucesso, erroRetorno);
             });

             return false;
         };

         function inicializaBandoDeDados() {

             if (!abrirDatabase()) {
                 return false;
             }

             //--- Cria o database caso não existe
             db = openDatabase(shortName, version, displayName, maxSize);

             //--- Cria a tabela
             db.transaction(function (tx) {
                 tx.executeSql('CREATE TABLE IF NOT EXISTS Treino(Id INTEGER NOT NULL PRIMARY KEY, NivelDeDificuldade TEXT, professor TEXT, ObjetivoTreino TEXT, Nome TEXT, DataDeCriacao DATETIME, DataDeAlteracao DATETIME)', [], nuloRetorno, erroRetorno);
                 tx.executeSql('CREATE TABLE IF NOT EXISTS Treino(Id INTEGER NOT NULL PRIMARY KEY, NivelDeDificuldade TEXT, professor TEXT, ObjetivoTreino TEXT, Nome TEXT, DataDeCriacao DATETIME, DataDeAlteracao DATETIME)', [], nuloRetorno, erroRetorno);
             }, erroRetorno, nuloRetorno);

             return true;
         };

         function abrirDatabase() {

             //--- Verifica se funciona neste browser
             if (!window.openDatabase) {
                 navigator.notification.alert(
                     'Não é possível abrir o banco de dados no seu celular.', // message
                      alertDismissed,                            // callback
                     'Workout App',                                           // title
                     'OK'                                                     // buttonName
                 );
                 return false;
             }

             return true;
         };

         function salvarSucesso() {
             navigator.notification.alert(
                 'Senha cadastrada com sucesso',                     // message
                  alertDismissed,                       // callback
                 'Workout App',                                    // title
                 'OK'                                                // buttonName
             );
         };

         function erroRetorno(error) {
             navigator.notification.alert(
                 'Erro: ' + error.message + ' cod: ' + error.code,   // message
                  alertDismissed,                       // callback
                 'Workout App',                                      // title
                 'OK'                                                // buttonName
             );
         };

         function nuloRetorno() { };

         function alertDismissed() { };

         return dataFactory;
     }]);