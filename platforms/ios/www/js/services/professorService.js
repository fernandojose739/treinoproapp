﻿'use strict';
marombaApp.factory('professorService', ['configApp', '$http',
     function (configApp, $http) {

         var dataFactory = {};

         dataFactory.listar = function (usuarioId, professorNome) {

             if (professorNome === '') {
                 professorNome = 'todosProfessores';
             }

             return $http.get(configApp.serverUrl + '/api/professor/listar/' + usuarioId + '/' + professorNome);
         };

         dataFactory.salvarUsuarioProfessor = function (usuarioProfessor) {

             var req = {
                 method: 'POST',
                 url: configApp.serverUrl + '/api/professor/SalvarUsuarioProfessor',
                 headers: { 'Content-Type': 'application/json' },
                 data: JSON.stringify(usuarioProfessor),
             }

             return $http(req);
         };

         dataFactory.removerUsuarioProfessor = function (usuarioProfessor) {

             var req = {
                 method: 'POST',
                 url: configApp.serverUrl + '/api/professor/RemoverUsuarioProfessor',
                 headers: { 'Content-Type': 'application/json' },
                 data: JSON.stringify(usuarioProfessor),
             }

             return $http(req);
         };

         return dataFactory;
     }]);