﻿'use strict';
marombaApp.controller('anamneseController', [
    '$routeParams', '$rootScope', '$scope', 'baseService', 'configApp', 'sessionService',
    function ($routeParams, $rootScope, $scope, baseService, configApp, sessionService) {

        sessionService.validarSession();

        initVarGerais();
        obter();

        function obter() {

            $scope.carregando = true;

            var method = 'GET';
            var url = configApp.serverUrl + '/api/anamnese/obter/' + $rootScope.usuario.Id;

            baseService.invoke(method, url, '')
                .success(function (data) {
                    if (data != null) {
                        $scope.formEditar.id = data.Id;
                        $scope.formEditar.dataDeCriacao = new Date(data.DataDeCriacao);
                        $scope.formEditar.dataDeNascimento = new Date(data.DataDeNascimento);
                        $scope.formEditar.atividadesDaVidaDiaria = data.AtividadesDaVidaDiaria;
                        $scope.formEditar.historicoMedico = data.HistoricoMedico;
                        $scope.formEditar.alergia = data.Alergia;
                        $scope.formEditar.acidente = data.Acidente;
                        $scope.formEditar.restricaoAtividadeFisica = data.RestricaoAtividadeFisica;
                        $scope.formEditar.fumante = data.Fumante;
                        $scope.formEditar.objetivo = data.Objetivo;
                        $scope.formEditar.realizaAtividadeFisica = data.RealizaAtividadeFisica;
                        $scope.formEditar.comentariosGerais = data.ComentariosGerais;
                        $scope.formEditar.diasParaTreinar = data.DiasParaTreinar;
                        $scope.formEditar.horasParaTreinarPorDia = data.HorasParaTreinarPorDia;
                        $scope.formEditar.usuarioId = $rootScope.usuario.Id;
                    } else {
                        initForm();
                    }

                    $scope.carregando = false;
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                });
        };

        $scope.salvar = function () {

            $scope.carregando = true;

            var method = 'POST';
            var url = configApp.serverUrl + '/api/anamnese/salvar';

            baseService.invoke(method, url, $scope.formEditar)
                 .success(function (response) {
                     if (response.Erros.length == 0) {
                         $scope.status = 'Salvo com sucesso';
                     } else {
                         $scope.status = response.Erros[0].Mensagem;
                     }

                     $scope.carregando = false;
                 })
                 .error(function (erro) {
                     $scope.carregando = false;
                     $scope.temRegistro = false;
                     $scope.status = 'Erro ao salvar';
                 });
        };

        function initForm() {
            $scope.formEditar.id = '';
            $scope.formEditar.dataDeCriacao = new Date();
            $scope.formEditar.dataDeNascimento = new Date();
            $scope.formEditar.atividadesDaVidaDiaria = '';
            $scope.formEditar.historicoMedico = '';
            $scope.formEditar.alergia = '';
            $scope.formEditar.acidente = '';
            $scope.formEditar.restricaoAtividadeFisica = '';
            $scope.formEditar.fumante = '';
            $scope.formEditar.objetivo = '';
            $scope.formEditar.realizaAtividadeFisica = '';
            $scope.formEditar.comentariosGerais = '';
            $scope.formEditar.diasParaTreinar = '5';
            $scope.formEditar.horasParaTreinarPorDia = '1';
            $scope.formEditar.usuarioId = $rootScope.usuario.Id;
        };

        function initVarGerais() {
            $scope.formEditar = {};
            $scope.status = '';
            $scope.carregando = false;
        };
    }
]);