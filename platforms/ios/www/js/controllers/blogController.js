﻿'use strict';
marombaApp.controller('blogController', [
    '$routeParams', '$rootScope', '$scope', 'baseService', 'sessionService', 'configApp',
    function ($routeParams, $rootScope, $scope, baseService, sessionService, configApp) {

        sessionService.validarSession();

        $scope.temRegistro = false;
        $scope.status = '';
        $scope.blogs = {};
        $scope.carregando = false;
        $scope.detalhe = false;

        $scope.listar = function () {

            $scope.carregando = true;

            var method = 'GET';
            var url = configApp.serverUrl + '/api/blog/listar/';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    $scope.blogs = data;
                    $scope.temRegistro = true;
                    $scope.carregando = false;
                })
                .error(function (error) {
                    if (error != null)
                        $scope.status = 'Erro ao carregar: ' + error.message;

                    $scope.temRegistro = false;
                    $scope.carregando = false;
                });
        };

        $scope.selecionar = function (blog) {
            $scope.blog = blog;
            $scope.detalhe = true;
        };

        $scope.voltar = function () {
            $scope.detalhe = false;
        }

        $scope.listar();
    }
]);