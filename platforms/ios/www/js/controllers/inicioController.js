﻿'use strict';
marombaApp.controller('inicioController', [
    '$routeParams', 'baseService', '$rootScope', '$scope', 'configApp', 'localStorageService', '$location', 'sessionService',
    function ($routeParams, baseService, $rootScope, $scope, configApp, localStorageService, $location, sessionService) {

        sessionService.validarSession();

        function initVarGerais() {
            $scope.carregando = true;
            $scope.temTreino = false;
            $scope.temMedidas = false;
            $scope.semTreino = false;
            $scope.semMedidas = false;
            $rootScope.treino = {};
            $scope.medidas = {};
            $scope.carregarDicas = true;
        };

        function obterTreino() {

            //--- cache
            var treino = localStorageService.obter("treino");
            if (treino != undefined) {
                $rootScope.treino = treino;
                $scope.temTreino = true;
                $scope.semTreino = false;
                return;
            }

            //--- db
            var method = "GET";
            var url = configApp.serverUrl + "/api/treino/listarTreinoAtivo/" + $rootScope.usuario.Id;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $rootScope.treino = data[0];
                        $scope.temTreino = true;
                        $scope.semTreino = false;
                        localStorageService.salvar("treino", data[0]);

                    } else {
                        $scope.temTreino = false;
                        $scope.semTreino = true;
                    }
                })
                .error(function () {
                    $scope.temTreino = false;
                    $scope.semTreino = true;
                });
        };

        function obterMedidas() {

            //--- cache
            var medidas = localStorageService.obter("medidas");
            if (medidas != undefined) {
                $scope.medidas = medidas;
                $scope.temMedidas = true;
                $scope.semMedidas = false;
                $scope.carregando = false;
                return;
            }

            var method = "GET";
            var url = configApp.serverUrl + "/api/medidas/listar/" + $rootScope.usuario.Id;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.medidas = data;
                        $scope.temMedidas = true;
                        $scope.semMedidas = false;
                        localStorageService.salvar("medidas", data);
                    } else {
                        $scope.temMedidas = false;
                        $scope.semMedidas = true;
                    }

                    $scope.carregando = false;
                })
                .error(function () {
                    $scope.carregando = false;
                    $scope.temMedidas = false;
                    $scope.semMedidas = true;
                });
        };

        function obterExercicios() {

            if ($rootScope.carregouNotificacao == undefined) {
                $rootScope.carregouNotificacao = false;
            }

            if ($rootScope.carregouNotificacao === true) {
                return;
            }

            var method = "GET";
            var url;

            url = configApp.serverUrl + "/api/exercicio/listarPorGrupoMuscular/1";
            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        localStorageService.salvar("ExerciciosDePeito", data);
                    }
                })
                .error(function () { });

            url = configApp.serverUrl + "/api/exercicio/listarPorGrupoMuscular/2";
            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        localStorageService.salvar("ExerciciosDeCostas", data);
                    }
                })
                .error(function () { });

            url = configApp.serverUrl + "/api/exercicio/listarPorGrupoMuscular/3";
            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        localStorageService.salvar("ExerciciosDePerna", data);
                    }
                })
                .error(function () { });

            url = configApp.serverUrl + "/api/exercicio/listarPorGrupoMuscular/4";
            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        localStorageService.salvar("ExerciciosDeOmbro", data);
                    }
                })
                .error(function () { });

            url = configApp.serverUrl + "/api/exercicio/listarPorGrupoMuscular/5";
            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        localStorageService.salvar("ExerciciosDeBiceps", data);
                    }
                })
                .error(function () { });

            url = configApp.serverUrl + "/api/exercicio/listarPorGrupoMuscular/6";
            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        localStorageService.salvar("ExerciciosDeTriceps", data);
                    }
                })
                .error(function () { });

            url = configApp.serverUrl + "/api/exercicio/listarPorGrupoMuscular/7";
            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        localStorageService.salvar("ExerciciosDeAbdominal", data);
                    }
                })
                .error(function () { });

            url = configApp.serverUrl + "/api/exercicio/listarPorGrupoMuscular/8";
            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        localStorageService.salvar("ExerciciosDeAerobico", data);
                    }
                })
                .error(function () { });
        }

        function obterNotificacoes() {

            $rootScope.temNotificacao = false;

            if ($rootScope.carregouNotificacao == undefined) {
                $rootScope.carregouNotificacao = false;
            }

            if ($rootScope.carregouNotificacao === true) {
                return;
            }

            $rootScope.carregandoNotificacao = true;

            var usuario = localStorageService.obter('usuario');
            var method = 'GET';
            var url = configApp.serverUrl + 'api/Notificacao/listar/' + usuario.Id;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $rootScope.notificacoes = data;

                        angular.forEach(data, function (notificacao) {
                            if (notificacao.Lida === false) {
                                $rootScope.temNotificacao = true;
                            }
                        });
                    }

                    $rootScope.carregouNotificacao = true;
                    $rootScope.carregandoNotificacao = false;
                })
                .error(function () {
                    $rootScope.carregandoNotificacao = false;
                });
        };

        function obterDicas() {

            $scope.carregandoDicas = true;

            //--- cache
            var dicas = localStorageService.obter("dicas");
            if (dicas != undefined && !$scope.carregarDicas) {
                $scope.dicas = dicas;
                $scope.dica = dicas[Math.floor(Math.random() * dicas.length)];
                $scope.carregandoDicas = false;
                return;
            }

            var method = "GET";
            var url = configApp.serverUrl + "/api/dica/listar/";

            baseService.invoke(method, url, "")
                .success(function (data) {

                    if (data.length > 0) {
                        $scope.dicas = data;
                        $scope.dica = data[Math.floor(Math.random() * data.length)];
                        localStorageService.salvar("dicas", data);
                        $scope.carregarDicas = false;
                    }

                    $scope.carregandoDicas = false;
                })
                .error(function () {
                    $scope.carregandoDicas = false;
                });
        };

        $scope.treinar = function () {
            $location.url("/treinoAtivo");
        };

        $scope.lerNotificacao = function () {

            if (!$rootScope.temNotificacao) {
                return;
            }

            var method = 'POST';
            var url = configApp.serverUrl + '/api/Notificacao/lerNotificacao';
            var notificacaoViewModel = {};
            notificacaoViewModel.AvisarUsuarioId = $rootScope.usuario.Id;

            baseService.invoke(method, url, notificacaoViewModel)
                 .success(function () { })
                 .error(function () { });

            $rootScope.temNotificacao = false;
        };

        initVarGerais();
        obterTreino();
        obterMedidas();
        obterExercicios();
        obterNotificacoes();
        obterDicas();
    }
]);