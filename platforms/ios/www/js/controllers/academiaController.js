﻿'use strict';
marombaApp.controller('academiaController', [
    '$routeParams', '$rootScope', '$scope', 'academiaService', 'configApp', 'sessionService',
    function ($routeParams, $rootScope, $scope, academiaService, configApp, sessionService) {

        sessionService.validarSession();

        $scope.status = '';
        $scope.nome = '';
        $scope.temRegistro = false;
        $scope.adicionar = false;
        $scope.academias = {};
        $scope.academiaForm = {};
        $scope.academiaForm.nome = '';
        $scope.academiaForm.cidade = '';
        $scope.academiaForm.bairro = '';
        $scope.academiaForm.usuarioId = $rootScope.usuario.Id;
        $scope.carregando = false;
        $scope.ordenarPor = 'nome';

        $scope.cancelar = function() {
            $scope.adicionar = false;
        };

        $scope.listar = function () {

            $scope.carregando = true;

            academiaService.listar($rootScope.usuario.Id, $scope.nome)
                .success(function (data) {

                    if (data.length > 0) {
                        $scope.academias = data;
                        $scope.temRegistro = true;
                        $scope.adicionar = false;
                        $scope.ordenarPor = 'nome';
                    } else {
                        $scope.academiaForm.nome = $scope.nome;
                        $scope.temRegistro = false;
                        $scope.adicionar = true;
                    }

                    $scope.carregando = false;
                })
                .error(function (error) {

                    if (error != null)
                        $scope.status = 'Erro ao carregar as academias';

                    $scope.temRegistro = false;
                    $scope.carregando = false;
                });
        };

        $scope.selecionar = function (academiaId) {

            $scope.carregando = true;

            var academia = {};
            academia.Id = academiaId;
            academia.UsuarioId = $rootScope.usuario.Id;

            var url = configApp.serverUrl + '/api/academia/SalvarAcademiaUsuario';

            academiaService.adicionar(url, academia)
                 .success(function (data) {
                     $scope.listar();
                 })
                 .error(function (data) {
                     $scope.carregando = false;
                 });
        };

        $scope.remover = function () {

            $scope.carregando = true;

            var usuario = {};
            usuario.Id = $rootScope.usuario.Id;

            academiaService.removerAcademiaUsuario(usuario)
                 .success(function (data) {
                     $scope.listar();
                 })
                 .error(function (error) {
                     $scope.carregando = false;
                 });
        };

        $scope.adicionarAcademia = function () {

            $scope.carregando = true;

            if ($scope.academiaForm.nome == '' || $scope.academiaForm.cidade == '' || $scope.academiaForm.bairro == '') {
                $scope.carregando = false;
                return false;
            }

            var url = configApp.serverUrl + '/api/academia/Adicionar';
            
            academiaService.adicionar(url, $scope.academiaForm)
                 .success(function (response) {
                     if (response[0].Erros.length == 0) {
                         $scope.academias = response;
                         $scope.temRegistro = true;
                         $scope.adicionar = false;
                         $scope.status = '';
                     } else {
                         $scope.status = response[0].Erros[0].Mensagem;
                     }

                     $scope.carregando = false;
                 })
                 .error(function (erro) {
                     $scope.carregando = false;
                     $scope.temRegistro = false;
                     $scope.status = 'Erro ao adicionar academia';
                 });
        };
    }
]);