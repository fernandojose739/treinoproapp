﻿'use strict';
marombaApp.controller('historicoController', [
    '$routeParams', '$rootScope', '$scope', 'baseService', 'configApp', 'sessionService', 'localStorageService',
    function ($routeParams, $rootScope, $scope, baseService, configApp, sessionService, localStorageService) {

        sessionService.validarSession();

        function obterHistorico() {

            $scope.carregando = true;

            var treino = localStorageService.obter('treino');
            var method = 'GET';
            var url = configApp.serverUrl + '/api/treino/obterHistorico/' + treino.Id;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data != null) {
                        $scope.temRegistro = true;
                        $scope.historicos = data;
                    } else {
                        $scope.temRegistro = false;
                    }

                    $scope.carregando = false;
                })
                .error(function (error) { });
        };
        
        obterHistorico();
    }
]);