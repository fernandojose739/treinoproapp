﻿'use strict';

var marombaApp = angular.module('marombaApp', ['ngRoute', 'mobile-angular-ui', 'ui.bootstrap']);

marombaApp.constant("configApp", {
    "serverUrl": "http://www.treinopro.com.br",
    "port": "80"
});

//http://localhost:29659
//http://www.treinopro.com.br
//http://treinoproapp.azurewebsites.net