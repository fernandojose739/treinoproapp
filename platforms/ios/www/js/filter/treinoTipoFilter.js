﻿'use strict';

marombaApp.filter('treinoTipoFilter', treinoTipo);

function treinoTipo() {
    return function (treino) {
        var treinoTipoDia = '';

        if (treino.ExerciciosTreinoA != undefined && treino.ExerciciosTreinoA.length > 0) {
            treinoTipoDia += 'A';
        }

        if (treino.ExerciciosTreinoB != undefined && treino.ExerciciosTreinoB.length > 0) {
            treinoTipoDia += 'B';
        }

        if (treino.ExerciciosTreinoC != undefined && treino.ExerciciosTreinoC.length > 0) {
            treinoTipoDia += 'C';
        }

        if (treino.ExerciciosTreinoD != undefined && treino.ExerciciosTreinoD.length > 0) {
            treinoTipoDia += 'D';
        }

        if (treino.ExerciciosTreinoE != undefined && treino.ExerciciosTreinoE.length > 0) {
            treinoTipoDia += 'E';
        }

        if (treino.ExerciciosTreinoF != undefined && treino.ExerciciosTreinoF.length > 0) {
            treinoTipoDia += 'F';
        }

        return treinoTipoDia;
    }
}