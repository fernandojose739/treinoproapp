﻿'use strict'

marombaApp.filter('boolFilter', formatBool);

function formatBool() {
    return function (value) {
        if (value === true)
            return "Sim";
        else
            return "Não";
    }
}