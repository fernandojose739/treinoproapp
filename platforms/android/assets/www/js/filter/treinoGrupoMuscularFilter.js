﻿'use strict';

marombaApp.filter('treinoGrupoMuscularFilter', treinoTipo);

function treinoTipo() {
    return function (exercicios) {
        var grupoMuscular = '';
        var separador = '';

        angular.forEach(exercicios, function (exercicio) {
            if (grupoMuscular.toString().toUpperCase().indexOf(exercicio.Exercicio.GrupoMuscular.Descricao.toUpperCase()) < 0) {
                grupoMuscular = grupoMuscular + separador + exercicio.Exercicio.GrupoMuscular.Descricao;
                separador = " | ";
            }
        });

        return grupoMuscular;
    }
}