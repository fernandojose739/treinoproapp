﻿'use strict';
marombaApp.factory('academiaService', ['configApp', '$http',
     function (configApp, $http) {

         var dataFactory = {};

         dataFactory.listar = function (usuarioId, academiaNome) {

             if (academiaNome === '') {
                 academiaNome = 'todos';
             }

             return $http.get(configApp.serverUrl + '/api/academia/listar/' + usuarioId + '/' + academiaNome);
         };

         dataFactory.salvarAcademiaUsuario = function (usuarioId, academiaId) {

             var req = {
                 method: 'POST',
                 url: configApp.serverUrl + '/api/academia/SalvarAcademiaUsuario',
                 headers: { 'Content-Type': 'application/json' },
                 data: { usuarioId: usuarioId, academiaId: academiaId },
             }

             return $http(req);
         };

         dataFactory.removerAcademiaUsuario = function (usuario) {

             var req = {
                 method: 'POST',
                 url: configApp.serverUrl + '/api/academia/RemoverAcademiaUsuario',
                 headers: { 'Content-Type': 'application/json' },
                 data: JSON.stringify(usuario),
             }

             return $http(req);
         };

         dataFactory.adicionar = function (url, request) {

             var req = {
                 method: 'POST',
                 url: url,
                 headers: { 'Content-Type': 'text/json' },
                 data: JSON.stringify(request),
             }

             return $http(req);
         };

         return dataFactory;
     }]);