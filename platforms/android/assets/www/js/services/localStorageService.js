﻿'use strict';
marombaApp.factory('localStorageService', [
     function () {

         var dataFactory = {};

         dataFactory.salvar = function (chave, valor) {
             localStorage.setItem(chave, JSON.stringify(valor));
         };

         dataFactory.obter = function (chave) {
             var valor = JSON.parse(localStorage.getItem(chave));
             return valor;
         }

         dataFactory.remover = function (chave) {
             localStorage.removeItem(chave);
         }

         return dataFactory;
     }]);