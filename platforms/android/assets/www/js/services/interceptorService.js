﻿marombaApp.factory('myHttpResponseInterceptor', ['$q', '$location', function ($q, $location) {
    return {
        request: function (config) {
            //console.log(config);
            return config;
        },

        response: function (result) {
            return result;
        },

        responseError: function (rejection) {
            //console.log('Failed with', rejection.status, 'status');

            if (rejection.status == 403) {
                console.log('Acesso negado');
                $location.url('/Error/403');
            }

            if (rejection.status == 404) {
                console.log('Página não encontrada');
                $location.url('/Error/404');
            }

            if (rejection.status == 500) {
                console.log('Erro no servidor');
                $location.url('/Error/500');
            }

            return $q.reject(rejection);
        }
    }
}]);