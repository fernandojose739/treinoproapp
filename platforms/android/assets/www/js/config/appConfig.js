﻿marombaApp.config(['$httpProvider', '$routeProvider',
  function ($httpProvider, $routeProvider) {

      $httpProvider.interceptors.push('myHttpResponseInterceptor');

      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];

      $routeProvider.
          when('/blog', {
              templateUrl: 'views/blog/index.html',
              controller: 'blogController'
          }).
          when('/login', {
              templateUrl: 'views/login/index.html',
              controller: 'loginController'
          }).
          when('/treinos', {
              templateUrl: 'views/treino/index.html',
              controller: 'treinoController',
          }).
          when('/treino/:alunoNome/:alunoId/:professorId', {
              templateUrl: 'views/treino/index.html',
              controller: 'treinoController',
          }).
          when('/treino/:alunoNome/:alunoId/:professorId/:treinoId', {
              templateUrl: 'views/treino/index.html',
              controller: 'treinoController',
          }).
          when('/treinoAtivo', {
              templateUrl: 'views/treino/ativo.html',
              controller: 'treinoAtivoController'
          }).
          when('/historico', {
              templateUrl: 'views/treino/historico.html',
              controller: 'historicoController'
          }).
          when('/treinoBuscar', {
              templateUrl: 'views/treino/buscar.html',
              controller: 'treinoBuscarController'
          }).
          when('/professor', {
              templateUrl: 'views/professor/index.html',
              controller: 'professorController'
          }).
          when('/academia', {
              templateUrl: 'views/academia/index.html',
              controller: 'academiaController'
          }).
          when('/anamnese', {
              templateUrl: 'views/anamnese/index.html',
              controller: 'anamneseController'
          }).
          when('/medidas', {
              templateUrl: 'views/medidas/index.html',
              controller: 'medidasController'
          }).
          when('/alunos', {
              templateUrl: 'views/alunos/index.html',
              controller: 'alunosController'
          }).
          when('/inicio', {
              templateUrl: 'views/inicio/index.html',
              controller: 'inicioController'
          }).
          otherwise({
              redirectTo: '/login'
          });
  }]);

marombaApp.run([
    '$rootScope', '$window', function ($rootScope, $window) {

        $rootScope.usuario = {};
        $rootScope.usuario.Id = 0;
        $rootScope.usuario.Nome = "";
        $rootScope.usuario.FacebookId = "";
        $rootScope.usuario.Email = "";
        $rootScope.usuario.Logado = false;
    }
]);
