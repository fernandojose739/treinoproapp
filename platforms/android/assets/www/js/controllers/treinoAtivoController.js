﻿'use strict';
marombaApp.controller('treinoAtivoController', [
    '$routeParams', 'baseService', '$rootScope', '$scope', 'configApp', 'localStorageService', 'sessionService',
    function ($routeParams, baseService, $rootScope, $scope, configApp, localStorageService, sessionService) {

        sessionService.validarSession();

        function callBackAlert() { };

        function initVarGerais() {
            $scope.treino = {};
            $scope.temRegistro = false;
            $scope.ordenarPorOrdem = "Ordem";
        };

        function listar() {
            $scope.carregando = true;

            //--- cache
            var treino = localStorageService.obter('treino');
            if (treino != undefined) {

                $scope.treino = treino;

                $scope.treino.exerciciosTreino = [];

                if (treino.ExerciciosTreinoA.length > 0) {
                    $scope.treino.exerciciosTreino.push({
                        treino: 'Treino A',
                        exercicios: treino.ExerciciosTreinoA
                    });
                }

                if (treino.ExerciciosTreinoB.length > 0) {
                    $scope.treino.exerciciosTreino.push({
                        treino: 'Treino B',
                        exercicios: treino.ExerciciosTreinoB
                    });
                }

                if (treino.ExerciciosTreinoC.length > 0) {
                    $scope.treino.exerciciosTreino.push({
                        treino: 'Treino C',
                        exercicios: treino.ExerciciosTreinoC
                    });
                }

                if (treino.ExerciciosTreinoD.length > 0) {
                    $scope.treino.exerciciosTreino.push({
                        treino: 'Treino D',
                        exercicios: treino.ExerciciosTreinoD
                    });
                }

                if (treino.ExerciciosTreinoE.length > 0) {
                    $scope.treino.exerciciosTreino.push({
                        treino: 'Treino E',
                        exercicios: treino.ExerciciosTreinoE
                    });
                }

                if (treino.ExerciciosTreinoF.length > 0) {
                    $scope.treino.exerciciosTreino.push({
                        treino: 'Treino F',
                        exercicios: treino.ExerciciosTreinoF
                    });
                }

                $scope.carregando = false;
                $scope.temRegistro = true;

                return;
            }

            //--- nunca vai chegar aqui
            var method = 'GET';
            var url = configApp.serverUrl + '/api/treino/listarTreinoAtivo/' + $rootScope.usuario.Id;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {

                        var treino = data[0];

                        $scope.treino = treino;

                        $scope.treino.exerciciosTreino = [];

                        if (treino.ExerciciosTreinoA.length > 0) {
                            $scope.treino.exerciciosTreino.push({
                                treino: 'Treino A',
                                exercicios: treino.ExerciciosTreinoA
                            });
                        }

                        if (treino.ExerciciosTreinoB.length > 0) {
                            $scope.treino.exerciciosTreino.push({
                                treino: 'Treino B',
                                exercicios: treino.ExerciciosTreinoB
                            });
                        }

                        if (treino.ExerciciosTreinoC.length > 0) {
                            $scope.treino.exerciciosTreino.push({
                                treino: 'Treino C',
                                exercicios: treino.ExerciciosTreinoC
                            });
                        }

                        if (treino.ExerciciosTreinoD.length > 0) {
                            $scope.treino.exerciciosTreino.push({
                                treino: 'Treino D',
                                exercicios: treino.ExerciciosTreinoD
                            });
                        }

                        if (treino.ExerciciosTreinoE.length > 0) {
                            $scope.treino.exerciciosTreino.push({
                                treino: 'Treino E',
                                exercicios: treino.ExerciciosTreinoE
                            });
                        }

                        if (treino.ExerciciosTreinoF.length > 0) {
                            $scope.treino.exerciciosTreino.push({
                                treino: 'Treino F',
                                exercicios: treino.ExerciciosTreinoF
                            });
                        }

                        $scope.carregando = false;
                        $scope.temRegistro = true;
                    } else {
                        $scope.temRegistro = false;
                    }
                })
                .error(function (error) {
                    $scope.carregando = false;
                    $scope.temRegistro = false;
                });
        };

        $scope.atualizarPeso = function (exercicioTreino) {
            $scope.carregando = true;

            var method = 'POST';
            var url = configApp.serverUrl + '/api/DiaTreino/AtualizarPeso';

            baseService.invoke(method, url, exercicioTreino)
                 .success(function (response) {
                     $scope.carregando = false;
                     localStorageService.remover('treino');
                     localStorageService.salvar('treino', $scope.treino);
                 })
                 .error(function (erro) {
                     $scope.carregando = false;
                 });
        };

        $scope.editarExercicio = function (exercicio) {
            exercicio.ExibirEditarExercicio = false;
        };

        $scope.cancelarEdicaoExercicio = function (exercicio) {
            exercicio.ExibirEditarExercicio = true;
        };

        $scope.salvarExercicio = function (exercicioTreino) {
            var method = 'POST';
            var url = configApp.serverUrl + '/api/DiaTreino/Atualizar';

            baseService.invoke(method, url, exercicioTreino)
                .success(function (response) {
                    localStorageService.remover('treino');
                    localStorageService.salvar('treino', $scope.treino);
                    $scope.cancelarEdicaoExercicio(exercicioTreino);
                    $scope.carregando = false;
                })
                .error(function (erro) {
                    $scope.carregando = false;
                });
        };

        $scope.treinoRealizado = function (exercicioTreino) {

            $scope.carregando = true;

            var method = 'POST';
            var url = configApp.serverUrl + '/api/DiaTreino/TreinoRealizado';
            var diaTreinoViewModel = {};
            diaTreinoViewModel.DiaId = exercicioTreino.exercicios[0].DiaId;
            diaTreinoViewModel.TreinoId = exercicioTreino.exercicios[0].TreinoId;

            baseService.invoke(method, url, diaTreinoViewModel)
                 .success(function (response) {
                     $scope.carregando = false;
                     navigator.notification.alert(
                            'Treino realizado com sucesso',
                            callBackAlert,
                            'Treino Realizado',
                            'OK'
                        );
                     $scope.carregando = false;
                 })
                 .error(function (erro) {
                     $scope.carregando = false;
                     navigator.notification.alert(
                            'Erro ao registrar treino realizado',
                            callBackAlert,
                            'Treino Realizado',
                            'OK'
                        );
                     $scope.carregando = false;
                 });
        };

        initVarGerais();
        listar();
    }
]);