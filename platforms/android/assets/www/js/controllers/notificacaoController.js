﻿'use strict';
marombaApp.controller('notificacaoController', [
    'baseService', 'configApp', 'localStorageService', '$rootScope', '$scope',
    function (baseService, configApp, localStorageService, $rootScope, $scope) {

        function callBackAlert() { };

        //function listar() {

        //    $scope.carregando = true;
        //    $rootScope.temNotificacao = false;

        //    var usuario = localStorageService.obter('usuario');
        //    var method = 'GET';
        //    var url = configApp.serverUrl + 'api/Notificacao/listar/' + usuario.Id;

        //    baseService.invoke(method, url, "")
        //        .success(function (data) {
        //            if (data.length > 0) {
        //                $scope.notificacoes = data;

        //                angular.forEach(data, function (notificacao) {
        //                    if (notificacao.Lida === false) {
        //                        $rootScope.temNotificacao = true;
        //                    }
        //                });
        //            }

        //            $scope.carregando = false;
        //        })
        //        .error(function (error) {
        //            $scope.carregando = false;
        //        });
        //};

        function marcarNotificacaoAtualizada(acao) {
            var method = 'GET';
            var url = configApp.serverUrl + '/api/Notificacao/marcarNotificacaoAtualizada/' + acao + '/' + $rootScope.usuario.Id;

            baseService.invoke(method, url, "")
                .success(function (data) { })
                .error(function (error) { });
        }

        $scope.atualizar = function (notificacao) {
            if (notificacao.Acao === "AtualizarListaDeTreinos") {

                $scope.carregando = true;

                var method = 'GET';
                var url = configApp.serverUrl + '/api/treino/listar/' + $rootScope.usuario.Id;

                baseService.invoke(method, url, "")
                    .success(function (data) {
                        if (data.length > 0) {
                            localStorageService.remover('treinos');
                            localStorageService.salvar('treinos', data);

                            //--- pega o treino ativo
                            angular.forEach(data, function (treino) {
                                if (treino.Ativo) {
                                    localStorageService.remover('treino');
                                    localStorageService.salvar('treino', treino);
                                    $rootScope.treino = treino;
                                }
                            });

                            //--- marca as notificacoes como lidas
                            angular.forEach($rootScope.notificacoes, function (notificacaoAux) {
                                notificacaoAux.Atualizada = true;
                            });
                        }

                        //--- Registra que as notificações já foram atualizadas
                        marcarNotificacaoAtualizada('AtualizarListaDeTreinos');

                        $scope.carregando = false;

                        navigator.notification.alert(
                            'Treino atualizado com sucesso',
                            callBackAlert,
                            'Atualização',
                            'OK'
                        );
                    })
                    .error(function (error) {
                        navigator.notification.alert(
                            'Erro ao atualizar o treino, favor tentar novamente mais tarde',
                            callBackAlert,
                            'Atualização',
                            'OK'
                        );
                    });
                return;
            }
        };

        //listar();
    }
]);