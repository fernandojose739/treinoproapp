﻿'use strict';
marombaApp.controller('treinoBuscarController', [
    '$routeParams', 'baseService', '$rootScope', '$scope', 'configApp', 'localStorageService', 'sessionService', '$location',
    function ($routeParams, baseService, $rootScope, $scope, configApp, localStorageService, sessionService, $location) {

        sessionService.validarSession();

        function callBackAlert() { };

        function listarNivelDeDificuldade() {
            $scope.carregando = true;

            var nivelDeDificuldades = localStorageService.obter('nivelDeDificuldades');
            if (nivelDeDificuldades != undefined) {
                $scope.nivelDeDificuldades = nivelDeDificuldades;
                $scope.carregando = false;
                return;
            }

            var method = 'GET';
            var url = configApp.serverUrl + '/api/treino/listarNivelDeDificuldade';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.nivelDeDificuldades = data;
                        localStorageService.salvar('nivelDeDificuldades', data);
                        $scope.carregando = false;
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                });
        };

        function listarObjetivoTreino() {

            $scope.carregando = true;

            var objetivoTreinos = localStorageService.obter('objetivoTreinos');
            if (objetivoTreinos != undefined) {
                $scope.objetivoTreinos = objetivoTreinos;
                $scope.carregando = false;
                return;
            }

            var method = 'GET';
            var url = configApp.serverUrl + '/api/ObjetivoTreino/listar';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.objetivoTreinos = data;
                        localStorageService.salvar('objetivoTreinos', data);
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                });
        };

        $scope.buscarTreino = function() {
            $scope.carregando = true;
            
            var method = 'GET';
            var url = configApp.serverUrl + 'api/treino/buscar/' + $scope.objetivoTreino.Id + '/' + $scope.nivelDeDificuldade.Id;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.treinos = data;
                        $scope.temRegistro = true;
                    }

                    $scope.carregando = false;
                })
                .error(function (error) {
                    $scope.carregando = false;
                    $scope.temRegistro = false;
                });
        };

        $scope.exibirTreinoToggle = function(treino) {
            $scope.temRegistro = !$scope.temRegistro;

            if (treino != null) {
                $scope.treino = treino;
            }
        };

        $scope.meusTreinos = function() {
            $location.url('/treinos');
        };

        $scope.selecionar = function(treino) {
            $scope.carregando = true;

            var method = 'POST';
            var url = configApp.serverUrl + '/api/treino/selecionar';

            baseService.invoke(method, url, treino)
                 .success(function (response) {
                     if (response.Erros.length === 0) {
                         
                         //--- adicionar treino na lista de treinos
                         var treinos = localStorageService.obter('treinos');
                         treinos.push(treino);
                         localStorageService.remover('treinos');
                         localStorageService.salvar('treinos', treinos);

                         listar();

                         //--- mensagem de alerta
                         navigator.notification.alert(
                            'Treino adicionado com sucesso',
                            callBackAlert,
                            'Adicionar Treino',
                            'OK'
                        );
                     } else {
                         $scope.status = 'Falha ao selecionar o treino, favor tentar novamente mais tarde.';
                         $scope.carregando = false;
                     }
                 })
                 .error(function () {
                     $scope.carregando = false;
                     $scope.temRegistro = false;
                     $scope.status = 'Erro ao salvar formulário';
                 });
        };

        listarNivelDeDificuldade();
        listarObjetivoTreino();
        $scope.temRegistro = false;
        $scope.exibirTreino = false;
        $scope.objetivoTreino = {};
        $scope.nivelDeDificuldade = {};
        $scope.treinos = {};
        $scope.treino = {};
    }
]);