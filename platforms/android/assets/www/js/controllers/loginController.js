﻿'use strict';
marombaApp.controller('loginController', [
    '$scope', 'localStorageService', '$location', 'baseService', 'configApp', '$rootScope', 'sessionService',
    function ($scope, localStorageService, $location, baseService, configApp, $rootScope, sessionService) {

        function carregarTreinoPrimeiroAcesso() {
            var method = 'GET';
            var url = configApp.serverUrl + '/Api/Treino/CarregarTreinosPrimeiroAcesso/' + $rootScope.usuario.Id + '/' + $rootScope.usuario.gender;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        localStorageService.salvar('treinos', data);

                        //--- pega o treino ativo
                        angular.forEach(data, function (treino) {
                            if (treino.Ativo) {
                                localStorageService.salvar('treino', treino);
                            }
                        });
                    }
                })
                .error(function () {
                });
        }

        //$rootScope.usuario = {};
        //$rootScope.usuario.Nome = "Fernando";
        //$rootScope.usuario.FacebookId = '804878756233743';
        //$rootScope.usuario.AcademiaId = 1;
        //$rootScope.usuario.Email = 'teste';
        //$rootScope.usuario.Logado = true;
        //$rootScope.usuario.Id = 6;
        //localStorageService.salvar('usuario', $rootScope.usuario);

        //--- verifica se o usuário já esta logado
        sessionService.usuarioLogado();

        //--- entrar no sistema
        $scope.entrar = function () {
            
            //--- login para iphone
            if ($scope.nome === '') {
                $scope.alerta = 'Nome é obrigatório';
                return;
            }

            $scope.carregando = true;

            //--- salva ou recupera o usuário na base
            var method = 'POST';
            var url = configApp.serverUrl + '/api/usuario/SalvarEObterUsuarioPorEmail';

            //--- busca os dados do usuário em minha base
            $rootScope.usuario = {};
            $rootScope.usuario.Nome = $scope.nome;
            $rootScope.usuario.FacebookId = '266324290204529';
            $rootScope.usuario.AcademiaId = 1;
            $rootScope.usuario.Email = $scope.nome;
            $rootScope.usuario.Logado = true;
            $rootScope.usuario.gender = 'homem';

            baseService.invoke(method, url, $rootScope.usuario)
                 .success(function (resultSalvarUsuario) {

                     $scope.primeiroAcesso = true;

                     $rootScope.usuario.Id = resultSalvarUsuario.Id;
                     localStorageService.salvar('usuario', $rootScope.usuario);

                     //--- Carrega os treinos de primeiro acesso
                     carregarTreinoPrimeiroAcesso();

                     $scope.carregando = false;
                 })
                .error(function (error) {
                    $scope.carregando = false;
                    $scope.primeiroAcesso = false;
                    alert(error.Exception);
                });

            return;
            //--- login para iphone fim

            //--- login do facebook
            facebookConnectPlugin.login(["public_profile"], function (data) {
                var user = {};
                user.fbid = data.authResponse.userID;

                //--- graph api
                facebookConnectPlugin.api(data.authResponse.userID + "/?fields=id,name,email,gender", ["public_profile"],
                    function (result) {

                        $scope.primeiroAcesso = true;

                        //--- busca os dados do usuário em minha base
                        $rootScope.usuario = {};
                        $rootScope.usuario.Nome = result.name;
                        $rootScope.usuario.FacebookId = result.id;
                        $rootScope.usuario.AcademiaId = 1;

                        $rootScope.usuario.Email = "sememail";
                        if (result.email != undefined && result.email !== '') {
                            $rootScope.usuario.Email = result.email;
                        }

                        $rootScope.usuario.Logado = true;

                        $rootScope.usuario.gender = 'homem';
                        if (result.gender != undefined && result.gender !== '') {

                            if (result.gender.toUpperCase() === 'MALE') {
                                $rootScope.usuario.gender = "homem";
                            }
                            else {
                                $rootScope.usuario.gender = "mulher";
                            }
                        }

                        //--- salva ou recupera o usuário na base
                        var method = 'POST';
                        var url = configApp.serverUrl + '/api/usuario/SalvarEObterUsuarioPorFacebook';

                        baseService.invoke(method, url, $rootScope.usuario)
                             .success(function (resultSalvarUsuario) {
                                 $rootScope.usuario.Id = resultSalvarUsuario.Id;
                                 localStorageService.salvar('usuario', $rootScope.usuario);

                                 //--- Carrega os treinos de primeiro acesso
                                 carregarTreinoPrimeiroAcesso();

                                 $scope.carregando = false;
                             })
                            .error(function (error) {
                                alert(error);
                                $scope.carregando = false;
                            });
                    },
                    function (error) {
                        $scope.carregando = false;
                        alert("Failed: " + error);
                    });
            }, function (error) {
                $scope.carregando = false;
                alert("error" + JSON.stringify(error));
            });
        };

        $scope.pular = function () {
            sessionService.usuarioLogado();
        };

        //--- Tour App
        $scope.slides = [];
        var slide = {
            image: 'boasVindas.jpg'
        };
        $scope.slides.push(slide);

        slide = {
            image: 'meustreinos.jpg'
        };
        $scope.slides.push(slide);

        slide = {
            image: 'treinoAtivoExercicio.jpg'
        };
        $scope.slides.push(slide);

        slide = {
            image: 'treinoAtivo.jpg'
        };
        $scope.slides.push(slide);

        slide = {
            image: 'notificacao.jpg'
        };
        $scope.slides.push(slide);

        slide = {
            image: 'menu.jpg'
        };
        $scope.slides.push(slide);

        slide = {
            image: 'inicio.jpg'
        };
        $scope.slides.push(slide);

        $scope.primeiroAcesso = false;
        $scope.direction = 'left';
        $scope.currentIndex = 0;

        $scope.setCurrentSlideIndex = function (index) {
            $scope.direction = (index > $scope.currentIndex) ? 'left' : 'right';
            $scope.currentIndex = index;
        };

        $scope.isCurrentSlideIndex = function (index) {
            return $scope.currentIndex === index;
        };

        $scope.prevSlide = function () {
            $scope.direction = 'left';
            $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;

            if ($scope.currentIndex === 1) {
                $scope.currentIndex = 0;
            }
        };

        $scope.nextSlide = function () {
            $scope.direction = 'right';
            $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;

            if ($scope.currentIndex === 0) {
                var usuarioLogado = sessionService.usuarioLogado();
                if (!usuarioLogado) {
                    $scope.exibirCarregando = true;
                    $scope.primeiroAcesso = false;
                }
            }
        };

        $scope.voltarAvancar = function (event) {

            var xTamanhoTotal = screen.width;
            var xPosicaoDoClique = event.offsetX;

            if ((xTamanhoTotal / 2) > xPosicaoDoClique) {
                $scope.prevSlide();
            } else {
                $scope.nextSlide();
            }

        };

        $scope.exibirCarregando = false;

        $scope.$watch("carregando", function (newValue, oldValue) {
            if ($scope.exibirCarregando && !newValue) {
                $scope.pular();
            }
        });

        $scope.nome = '';
        $scope.email = '';
        $scope.alerta = 'Esta tela só é exibida no primeiro acesso';
    }
]);