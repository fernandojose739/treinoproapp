﻿'use strict';
marombaApp.controller('treinoController', [
    '$routeParams', 'baseService', '$rootScope', '$scope', 'configApp', 'localStorageService', 'sessionService', '$location',
    function ($routeParams, baseService, $rootScope, $scope, configApp, localStorageService, sessionService, $location) {

        sessionService.validarSession();

        $scope.subMenuExibir = function () {
            $scope.submenu = !$scope.submenu;
        };

        $scope.subMenuEsconder = function () {
            $scope.submenu = false;
        };

        function init() {

            initVarGerais();

            var treinoId = $routeParams.treinoId;

            if (treinoId === -1) {
                $scope.souProfessor = true;
                initFormEditar();
                $scope.carregando = false;
                return;
            }

            if (treinoId != undefined && treinoId !== "" && treinoId > 0) {
                $scope.souProfessor = true;

                var method = 'GET';
                var url = configApp.serverUrl + '/api/treino/obter/' + treinoId;

                baseService.invoke(method, url, "")
                    .success(function (data) {

                        initFormEditar();
                        if (data != null) {
                            $scope.obter(data);
                        }

                        $scope.carregando = false;
                        $scope.exibirBotaoSalvar = true;
                    })
                    .error(function (error) {
                        if (error != null) $scope.status = 'Erro ao carregar';
                        $scope.carregando = false;
                    });
            } else {
                $scope.souProfessor = false;
                listar(false);
            }
        };

        function initVarGerais() {
            $scope.ordenarPor = 'ordem';
            $scope.exibirFormEditar = false;
            $scope.submenu = false;
            $scope.status = "";
            $scope.exibirBotaoSalvar = false;
            $scope.temRegistro = false;
            $scope.ordenarTreinoLista = 'Ativo';
        };

        function initFormEditar() {
            $scope.status = '';
            $scope.exibirFormEditar = true;
            $scope.nivelDeDificuldades = {};
            $scope.dias = {};
            $scope.objetivoTreinos = {};
            $scope.exibirBotaoSalvar = true;

            $scope.formCadastro = {};

            $scope.exibirFormAdicionarExercicio = false;
            $scope.formCadastro.adicionarExercicioMensagem = '';

            $scope.formCadastro.mensagemAdicionarExercicio = "";
            $scope.formCadastro.nome = "";
            $scope.formCadastro.objetivoTreino = "";
            $scope.formCadastro.nivelDeDificuldade = "";
            $scope.formCadastro.dia = {};

            $scope.formCadastro.dataDeCriacao = new Date();
            $scope.formCadastro.dataDeAlteracao = new Date();
            $scope.formCadastro.usuarioId = $rootScope.usuario.Id;
            $scope.formCadastro.usuarioIdCriadoPor = $rootScope.usuario.Id;

            var alunoNome = $routeParams.alunoNome;
            if (alunoNome != undefined && alunoNome !== "")
                $scope.formCadastro.usuarioNome = alunoNome;

            var alunoId = $routeParams.alunoId;
            if (alunoId != undefined && alunoId !== "")
                $scope.formCadastro.usuarioId = alunoId;

            var professorId = $routeParams.professorId;
            if (professorId != undefined && professorId !== "")
                $scope.formCadastro.usuarioIdCriadoPor = professorId;

            $scope.formCadastro.ativo = false;

            $scope.formCadastro.grupoMuscularesaA = {};
            $scope.formCadastro.grupoMuscularesaB = {};
            $scope.formCadastro.grupoMuscularesaC = {};
            $scope.formCadastro.grupoMuscularesaD = {};
            $scope.formCadastro.grupoMuscularesaE = {};
            $scope.formCadastro.grupoMuscularesaF = {};
            $scope.formCadastro.grupoMuscularesAE = {};
            $scope.formCadastro.grupoMuscularA = "";
            $scope.formCadastro.grupoMuscularB = "";
            $scope.formCadastro.grupoMuscularC = "";
            $scope.formCadastro.grupoMuscularD = "";
            $scope.formCadastro.grupoMuscularE = "";
            $scope.formCadastro.grupoMuscularF = "";

            initComboExercicios();

            limpaCampos('A');
            limpaCampos('B');
            limpaCampos('C');
            limpaCampos('D');
            limpaCampos('E');
            limpaCampos('F');

            $scope.formCadastro.exerciciosTreinoA = [];
            $scope.formCadastro.exerciciosTreinoB = [];
            $scope.formCadastro.exerciciosTreinoC = [];
            $scope.formCadastro.exerciciosTreinoD = [];
            $scope.formCadastro.exerciciosTreinoE = [];
            $scope.formCadastro.exerciciosTreinoF = [];

            $scope.treinoA = false;
            $scope.treinoB = false;
            $scope.treinoC = false;
            $scope.treinoD = false;
            $scope.treinoE = false;
            $scope.treinoF = false;

            //--- Carregar combos
            listarNivelDeDificuldade();
            listarObjetivoTreino();
            listarDias();
            listarGrupoMuscular();
        };

        function listarDias() {
            $scope.carregando = true;

            var dias = localStorageService.obter('dias');
            if (dias != undefined) {
                $scope.carregando = false;
                $scope.dias = dias;
                return;
            }

            var method = 'GET';
            var url = configApp.serverUrl + '/api/dia/listar';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.dias = data;
                        localStorageService.salvar('dias', data);
                    }

                    $scope.carregando = false;
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                });
        };

        function listarNivelDeDificuldade() {
            $scope.carregando = true;

            var nivelDeDificuldades = localStorageService.obter('nivelDeDificuldades');
            if (nivelDeDificuldades != undefined) {
                $scope.nivelDeDificuldades = nivelDeDificuldades;
                $scope.carregando = false;
                return;
            }

            var method = 'GET';
            var url = configApp.serverUrl + '/api/treino/listarNivelDeDificuldade';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.nivelDeDificuldades = data;
                        localStorageService.salvar('nivelDeDificuldades', data);
                        $scope.carregando = false;
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                });
        };

        function listar(temQueListar) {
            $scope.carregando = true;

            //--- cache
            if (!temQueListar) {
                var treinos = localStorageService.obter('treinos');
                if (treinos != undefined) {
                    $scope.carregando = false;
                    $scope.temRegistro = true;
                    $scope.ordenarTreinoLista = 'Ativo';
                    $scope.exibirFormEditar = false;
                    $scope.treinos = treinos;
                    return;
                }
            }

            var method = 'GET';
            var url = configApp.serverUrl + '/api/treino/listar/' + $rootScope.usuario.Id;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.treinos = data;
                        $scope.carregando = false;
                        $scope.temRegistro = true;
                        $scope.ordenarTreinoLista = 'Ativo';
                        $scope.exibirFormEditar = false;
                        localStorageService.salvar('treinos', data);

                        //--- pega o treino ativo
                        localStorageService.remover('treino');
                        angular.forEach(data, function (treino) {
                            if (treino.Ativo) {
                                localStorageService.salvar('treino', treino);
                                $rootScope.treino = treino;
                            }
                        });

                    } else {
                        localStorageService.remover('treino');
                        localStorageService.remover('treinos');
                        $scope.temRegistro = false;
                        $scope.ordenarTreinoLista = 'Ativo';
                        $scope.exibirFormEditar = false;
                        $scope.carregando = false;
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                    $scope.temRegistro = false;
                });
        };

        $scope.atualizarMeusTreinos = function () {
            listar(true);
            $scope.subMenuEsconder();
        };

        function listarGrupoMuscular() {

            $scope.carregando = true;

            var grupoMusculares = localStorageService.obter('grupoMusculares');
            if (grupoMusculares != undefined) {
                $scope.formCadastro.grupoMuscularesaA = grupoMusculares;
                $scope.formCadastro.grupoMuscularesaB = grupoMusculares;
                $scope.formCadastro.grupoMuscularesaC = grupoMusculares;
                $scope.formCadastro.grupoMuscularesaD = grupoMusculares;
                $scope.formCadastro.grupoMuscularesaE = grupoMusculares;
                $scope.formCadastro.grupoMuscularesaF = grupoMusculares;
                $scope.formCadastro.grupoMuscularesAE = grupoMusculares;
                $scope.carregando = false;
                return;
            }

            var method = 'GET';
            var url = configApp.serverUrl + '/api/GrupoMuscular/listar';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.formCadastro.grupoMuscularesaA = data;
                        $scope.formCadastro.grupoMuscularesaB = data;
                        $scope.formCadastro.grupoMuscularesaC = data;
                        $scope.formCadastro.grupoMuscularesaD = data;
                        $scope.formCadastro.grupoMuscularesaE = data;
                        $scope.formCadastro.grupoMuscularesaF = data;
                        $scope.formCadastro.grupoMuscularesAE = data;
                        localStorageService.salvar('grupoMusculares', data);
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                });
        };

        function preencheComboExercicios(data) {
            switch ($scope.formCadastro.dia.Descricao) {
                case "A":
                    $scope.formCadastro.exerciciosA = data;
                    break;

                case "B":
                    $scope.formCadastro.exerciciosB = data;
                    break;

                case "C":
                    $scope.formCadastro.exerciciosC = data;
                    break;

                case "D":
                    $scope.formCadastro.exerciciosD = data;
                    break;

                case "E":
                    $scope.formCadastro.exerciciosE = data;
                    break;

                case "F":
                    $scope.formCadastro.exerciciosF = data;
                    break;
            }
        }

        function listarObjetivoTreino() {

            $scope.carregando = true;

            var objetivoTreinos = localStorageService.obter('objetivoTreinos');
            if (objetivoTreinos != undefined) {
                $scope.objetivoTreinos = objetivoTreinos;
                $scope.carregando = false;
                return;
            }

            var method = 'GET';
            var url = configApp.serverUrl + '/api/ObjetivoTreino/listar';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.objetivoTreinos = data;
                        localStorageService.salvar('objetivoTreinos', data);
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                });
        };

        $scope.listarExerciciosPorGrupoMuscular = function () {

            $scope.carregando = true;

            initComboExercicios();

            var dia = $scope.formCadastro.dia.Descricao;
            var grupoMuscularId = $scope.formCadastro.grupoMuscularA.Id;

            switch (dia) {
                case "B":
                    grupoMuscularId = $scope.formCadastro.grupoMuscularB.Id;
                    break;

                case "C":
                    grupoMuscularId = $scope.formCadastro.grupoMuscularC.Id;
                    break;

                case "D":
                    grupoMuscularId = $scope.formCadastro.grupoMuscularD.Id;
                    break;

                case "E":
                    grupoMuscularId = $scope.formCadastro.grupoMuscularE.Id;
                    break;

                case "F":
                    grupoMuscularId = $scope.formCadastro.grupoMuscularF.Id;
                    break;
            }

            //--- recupera os exercicios do cache
            var exercicios = undefined;
            switch (grupoMuscularId) {
                case 1:
                    exercicios = localStorageService.obter("ExerciciosDePeito");
                    break;

                case 2:
                    exercicios = localStorageService.obter("ExerciciosDeCostas");
                    break;

                case 3:
                    exercicios = localStorageService.obter("ExerciciosDePerna");
                    break;

                case 4:
                    exercicios = localStorageService.obter("ExerciciosDeOmbro");
                    break;

                case 5:
                    exercicios = localStorageService.obter("ExerciciosDeBiceps");
                    break;

                case 6:
                    exercicios = localStorageService.obter("ExerciciosDeTriceps");
                    break;

                case 7:
                    exercicios = localStorageService.obter("ExerciciosDeAbdominal");
                    break;

                case 8:
                    exercicios = localStorageService.obter("ExerciciosDeAerobico");
                    break;
            }

            if (exercicios != undefined) {
                preencheComboExercicios(exercicios);
                $scope.carregando = false;
                return;
            }

            //--- busca na base
            var method = "GET";
            var url = configApp.serverUrl + "/api/exercicio/listarPorGrupoMuscular/" + grupoMuscularId;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        preencheComboExercicios(data);
                        $scope.carregando = false;
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = "Erro ao carregar";
                    $scope.carregando = false;
                });
        };

        function initComboExercicios() {
            $scope.formCadastro.exerciciosA = {};
            $scope.formCadastro.exerciciosB = {};
            $scope.formCadastro.exerciciosC = {};
            $scope.formCadastro.exerciciosD = {};
            $scope.formCadastro.exerciciosE = {};
            $scope.formCadastro.exerciciosF = {};
        };

        $scope.alterarDia = function () {

            var aba = $scope.formCadastro.dia.Descricao;

            $scope.treinoA = false;
            $scope.treinoB = false;
            $scope.treinoC = false;
            $scope.treinoD = false;
            $scope.treinoE = false;
            $scope.treinoF = false;
            if (aba == 'A') {
                $scope.treinoA = true;
                return;
            }

            if (aba == 'B') {
                $scope.treinoB = true;
                return;
            }

            if (aba == 'C') {
                $scope.treinoC = true;
                return;
            }

            if (aba == 'D') {
                $scope.treinoD = true;
                return;
            }

            if (aba == 'E') {
                $scope.treinoE = true;
                return;
            }

            if (aba == 'F') {
                $scope.treinoF = true;
                return;
            }
        }

        $scope.adicionarEditar = function (id) {

            $scope.carregando = true;
            $scope.submenu = false;

            if (id > 0) {
                $scope.obter(id);
            } else {
                initFormEditar();
                $scope.carregando = false;
            }
        };

        $scope.voltarParaLista = function () {
            $scope.exibirFormEditar = false;
            $scope.exibirBotaoSalvar = false;
            $scope.subMenuEsconder();
            listar(false);
        }

        $scope.salvar = function () {

            $scope.carregando = true;

            $scope.status = '';

            if (!validaCamposParaSalvar()) {
                $scope.carregando = false;
                return;
            }

            var method = 'POST';
            var url = configApp.serverUrl + '/api/treino/salvar';

            baseService.invoke(method, url, $scope.formCadastro)
                 .success(function (response) {
                     $scope.exibirBotaoSalvar = false;
                     if (response.Erros.length == 0) {
                         $scope.status = 'Sucesso ao salvar o treino';

                         if ($scope.souProfessor === true) {
                             $scope.voltarParaAlunos();
                         } else {
                             listar(true);
                             listarTreinoAtivo();
                         }
                     } else {
                         $scope.status = 'Erro ao salvar o treino';
                         $scope.carregando = false;
                     }
                 })
                 .error(function (erro) {
                     $scope.exibirBotaoSalvar = false;
                     $scope.carregando = false;
                     $scope.temRegistro = false;
                     $scope.status = 'Erro ao salvar formulário';
                 });
        };

        function listarTreinoAtivo() {
            //--- nunca vai chegar aqui
            var method = 'GET';
            var url = configApp.serverUrl + '/api/treino/listarTreinoAtivo/' + $rootScope.usuario.Id;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        var treino = data[0];
                        localStorageService.salvar('treino', treino);
                    }
                })
                .error(function (error) {

                });
        }

        function validaCamposParaSalvar() {
            if ($scope.formCadastro.nome == '') {
                $scope.status = 'Nome é obrigatório';
                return false;
            }

            if ($scope.formCadastro.objetivoTreinoId == 0) {
                $scope.status = 'Objetivo é obrigatório';
                return false;
            }

            if ($scope.formCadastro.nivelDeDificuldadeId == 0) {
                $scope.status = 'Nível de dificuldade é obrigatório';
                return false;
            }

            return true;
        }

        $scope.removerExercicioDeTreino = function (index) {

            var dia = $scope.formCadastro.dia.Descricao;

            if (dia == "A") {
                $scope.formCadastro.exerciciosTreinoA.splice(index, 1);
                return;
            }

            if (dia == "B") {
                $scope.formCadastro.exerciciosTreinoB.splice(index, 1);
                return;
            }

            if (dia == "C") {
                $scope.formCadastro.exerciciosTreinoC.splice(index, 1);
                return;
            }

            if (dia == "D") {
                $scope.formCadastro.exerciciosTreinoD.splice(index, 1);
                return;
            }

            if (dia == "E") {
                $scope.formCadastro.exerciciosTreinoE.splice(index, 1);
                return;
            }

            if (dia == "F") {
                $scope.formCadastro.exerciciosTreinoF.splice(index, 1);
                return;
            }
        };

        $scope.adicionarExercicio = function () {

            $scope.status = '';

            var dia = $scope.formCadastro.dia.Descricao;
            $scope.formCadastro.mensagemAdicionarExercicio = "";

            $scope.carregando = true;

            if (!validaCamposParaAdicionarExercicio()) {
                $scope.formCadastro.mensagemAdicionarExercicio = "Todos os campos são obrigátorio";
                $scope.carregando = false;
                return;
            }

            if (dia == "A") {

                var exerciciosTreinoA = {};
                exerciciosTreinoA.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoA.Exercicio = {};
                exerciciosTreinoA.Exercicio.Foto = $scope.formCadastro.exercicioA.Foto;
                exerciciosTreinoA.Exercicio.Descricao = $scope.formCadastro.exercicioA.Descricao;
                exerciciosTreinoA.Exercicio.Id = $scope.formCadastro.exercicioA.Id;
                exerciciosTreinoA.ExercicioId = $scope.formCadastro.exercicioA.Id;
                exerciciosTreinoA.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesA;
                exerciciosTreinoA.Repeticoes = $scope.formCadastro.repeticoesA;
                exerciciosTreinoA.Carga = $scope.formCadastro.cargaA;
                exerciciosTreinoA.Ordem = $scope.formCadastro.ordemA;
                exerciciosTreinoA.TempoDeDescando = $scope.formCadastro.tempoDeDescandoA;

                $scope.formCadastro.exerciciosTreinoA.push(exerciciosTreinoA);

                limpaCampos('A');
                $scope.carregando = false;
                return;
            }

            if (dia == "B") {
                var exerciciosTreinoB = {};
                exerciciosTreinoB.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoB.Exercicio = {};
                exerciciosTreinoB.Exercicio.Foto = $scope.formCadastro.exercicioB.Foto;
                exerciciosTreinoB.Exercicio.Descricao = $scope.formCadastro.exercicioB.Descricao;
                exerciciosTreinoB.Exercicio.Id = $scope.formCadastro.exercicioB.Id;
                exerciciosTreinoB.ExercicioId = $scope.formCadastro.exercicioB.Id;
                exerciciosTreinoB.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesB;
                exerciciosTreinoB.Repeticoes = $scope.formCadastro.repeticoesB;
                exerciciosTreinoB.Carga = $scope.formCadastro.cargaB;
                exerciciosTreinoB.Ordem = $scope.formCadastro.ordemB;
                exerciciosTreinoB.TempoDeDescando = $scope.formCadastro.tempoDeDescandoB;

                $scope.formCadastro.exerciciosTreinoB.push(exerciciosTreinoB);

                limpaCampos('B');
                $scope.carregando = false;
            }

            if (dia == "C") {

                var exerciciosTreinoC = {};
                exerciciosTreinoC.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoC.Exercicio = {};
                exerciciosTreinoC.Exercicio.Foto = $scope.formCadastro.exercicioC.Foto;
                exerciciosTreinoC.Exercicio.Descricao = $scope.formCadastro.exercicioC.Descricao;
                exerciciosTreinoC.Exercicio.Id = $scope.formCadastro.exercicioC.Id;
                exerciciosTreinoC.ExercicioId = $scope.formCadastro.exercicioC.Id;
                exerciciosTreinoC.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesC;
                exerciciosTreinoC.Repeticoes = $scope.formCadastro.repeticoesC;
                exerciciosTreinoC.Carga = $scope.formCadastro.cargaC;
                exerciciosTreinoC.Ordem = $scope.formCadastro.ordemC;
                exerciciosTreinoC.TempoDeDescando = $scope.formCadastro.tempoDeDescandoC;

                $scope.formCadastro.exerciciosTreinoC.push(exerciciosTreinoC);

                limpaCampos('C');
                $scope.carregando = false;
                return;
            }

            if (dia == "D") {

                var exerciciosTreinoD = {};
                exerciciosTreinoD.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoD.Exercicio = {};
                exerciciosTreinoD.Exercicio.Foto = $scope.formCadastro.exercicioD.Foto;
                exerciciosTreinoD.Exercicio.Descricao = $scope.formCadastro.exercicioD.Descricao;
                exerciciosTreinoD.Exercicio.Id = $scope.formCadastro.exercicioD.Id;
                exerciciosTreinoD.ExercicioId = $scope.formCadastro.exercicioD.Id;
                exerciciosTreinoD.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesD;
                exerciciosTreinoD.Repeticoes = $scope.formCadastro.repeticoesD;
                exerciciosTreinoD.Carga = $scope.formCadastro.cargaD;
                exerciciosTreinoD.Ordem = $scope.formCadastro.ordemD;
                exerciciosTreinoD.TempoDeDescando = $scope.formCadastro.tempoDeDescandoD;

                $scope.formCadastro.exerciciosTreinoD.push(exerciciosTreinoD);

                limpaCampos('D');
                $scope.carregando = false;
                return;
            }

            if (dia == "E") {

                var exerciciosTreinoE = {};
                exerciciosTreinoE.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoE.Exercicio = {};
                exerciciosTreinoE.Exercicio.Foto = $scope.formCadastro.exercicioE.Foto;
                exerciciosTreinoE.Exercicio.Descricao = $scope.formCadastro.exercicioE.Descricao;
                exerciciosTreinoE.Exercicio.Id = $scope.formCadastro.exercicioE.Id;
                exerciciosTreinoE.ExercicioId = $scope.formCadastro.exercicioE.Id;
                exerciciosTreinoE.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesE;
                exerciciosTreinoE.Repeticoes = $scope.formCadastro.repeticoesE;
                exerciciosTreinoE.Carga = $scope.formCadastro.cargaE;
                exerciciosTreinoE.Ordem = $scope.formCadastro.ordemE;
                exerciciosTreinoE.TempoDeDescando = $scope.formCadastro.tempoDeDescandoE;

                $scope.formCadastro.exerciciosTreinoE.push(exerciciosTreinoE);

                limpaCampos('E');
                $scope.carregando = false;
                return;
            }

            if (dia == "F") {

                var exerciciosTreinoF = {};
                exerciciosTreinoF.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoF.Exercicio = {};
                exerciciosTreinoF.Exercicio.Foto = $scope.formCadastro.exercicioF.Foto;
                exerciciosTreinoF.Exercicio.Descricao = $scope.formCadastro.exercicioF.Descricao;
                exerciciosTreinoF.Exercicio.Id = $scope.formCadastro.exercicioF.Id;
                exerciciosTreinoF.ExercicioId = $scope.formCadastro.exercicioF.Id;
                exerciciosTreinoF.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesF;
                exerciciosTreinoF.Repeticoes = $scope.formCadastro.repeticoesF;
                exerciciosTreinoF.Carga = $scope.formCadastro.cargaF;
                exerciciosTreinoF.Ordem = $scope.formCadastro.ordemF;
                exerciciosTreinoF.TempoDeDescando = $scope.formCadastro.tempoDeDescandoF;

                $scope.formCadastro.exerciciosTreinoF.push(exerciciosTreinoF);

                limpaCampos('F');
                $scope.carregando = false;
                return;
            }
        };

        function validaCamposParaAdicionarExercicio() {

            var dia = $scope.formCadastro.dia.Descricao;

            if ((dia == 'A') && ($scope.formCadastro.exercicioA == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesA == "" ||
                                 $scope.formCadastro.repeticoesA == "" ||
                                 $scope.formCadastro.cargaA == "" ||
                                 $scope.formCadastro.ordemA == "" ||
                                 $scope.formCadastro.tempoDeDescandoA == "")) {
                return false;
            }

            if ((dia == 'B') && ($scope.formCadastro.exercicioB == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesB == "" ||
                                 $scope.formCadastro.repeticoesB == "" ||
                                 $scope.formCadastro.cargaB == "" ||
                                 $scope.formCadastro.ordemB == "" ||
                                 $scope.formCadastro.tempoDeDescandoB == "")) {
                return false;
            }

            if ((dia == 'C') && ($scope.formCadastro.exercicioC == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesC == "" ||
                                 $scope.formCadastro.repeticoesC == "" ||
                                 $scope.formCadastro.cargaC == "" ||
                                 $scope.formCadastro.ordemC == "" ||
                                 $scope.formCadastro.tempoDeDescandoC == "")) {
                return false;
            }

            if ((dia == 'D') && ($scope.formCadastro.exercicioD == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesD == "" ||
                                 $scope.formCadastro.repeticoesD == "" ||
                                 $scope.formCadastro.cargaD == "" ||
                                 $scope.formCadastro.ordemD == "" ||
                                 $scope.formCadastro.tempoDeDescandoD == "")) {
                return false;
            }

            if ((dia == 'E') && ($scope.formCadastro.exercicioE == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesE == "" ||
                                 $scope.formCadastro.repeticoesE == "" ||
                                 $scope.formCadastro.cargaE == "" ||
                                 $scope.formCadastro.ordemE == "" ||
                                 $scope.formCadastro.tempoDeDescandoE == "")) {
                return false;
            }

            if ((dia == 'F') && ($scope.formCadastro.exercicioF == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesF == "" ||
                                 $scope.formCadastro.repeticoesF == "" ||
                                 $scope.formCadastro.cargaF == "" ||
                                 $scope.formCadastro.ordemF == "" ||
                                 $scope.formCadastro.tempoDeDescandoF == "")) {
                return false;
            }

            return true;
        }

        function limpaCampos(dia) {

            if (dia == 'A') {
                $scope.formCadastro.exercicioA = "";
                $scope.formCadastro.ordemA = parseFloat($scope.formCadastro.ordemA) + 1;
                return;
            }

            if (dia == 'B') {
                $scope.formCadastro.exercicioB = "";
                $scope.formCadastro.ordemB = parseFloat($scope.formCadastro.ordemB) + 1;
            }

            if (dia == 'C') {
                $scope.formCadastro.exercicioC = "";
                $scope.formCadastro.ordemC = parseFloat($scope.formCadastro.ordemC) + 1;
            }

            if (dia == 'D') {
                $scope.formCadastro.exercicioD = "";
                $scope.formCadastro.ordemD = parseFloat($scope.formCadastro.ordemD) + 1;
            }

            if (dia == 'E') {
                $scope.formCadastro.exercicioE = "";
                $scope.formCadastro.ordemE = parseFloat($scope.formCadastro.ordemE) + 1;
            }

            if (dia == 'F') {
                $scope.formCadastro.exercicioF = "";
                $scope.formCadastro.ordemF = parseFloat($scope.formCadastro.ordemF) + 1;
            }
        }

        $scope.obter = function (data) {

            $scope.carregando = true;

            initFormEditar();
            $scope.formCadastro.id = data.Id;
            $scope.formCadastro.dataDeCriacao = data.DataDeCriacao;
            $scope.formCadastro.dataDeAlteracao = new Date();
            $scope.formCadastro.nome = data.Nome;
            $scope.formCadastro.objetivoTreinoId = data.ObjetivoTreino.Id;
            $scope.formCadastro.nivelDeDificuldadeId = data.NivelDeDificuldade.Id;
            $scope.formCadastro.ativo = data.Ativo;
            $scope.formCadastro.exerciciosTreinoA = data.ExerciciosTreinoA;
            $scope.formCadastro.exerciciosTreinoB = data.ExerciciosTreinoB;
            $scope.formCadastro.exerciciosTreinoC = data.ExerciciosTreinoC;
            $scope.formCadastro.exerciciosTreinoD = data.ExerciciosTreinoD;
            $scope.formCadastro.exerciciosTreinoE = data.ExerciciosTreinoE;
            $scope.formCadastro.exerciciosTreinoF = data.ExerciciosTreinoF;

            $scope.carregando = false;
            $scope.exibirFormEditar = true;
            $scope.exibirBotaoSalvar = true;

            //var method = 'GET';
            //var url = configApp.serverUrl + '/api/treino/obter/' + id;

            //baseService.invoke(method, url, "")
            //    .success(function (data) {

            //        initFormEditar();
            //        if (data != null) {

            //            $scope.formCadastro.id = data.Id;
            //            $scope.formCadastro.dataDeCriacao = data.DataDeCriacao;
            //            $scope.formCadastro.dataDeAlteracao = new Date();
            //            $scope.formCadastro.nome = data.Nome;
            //            $scope.formCadastro.objetivoTreinoId = data.ObjetivoTreino.Id;
            //            $scope.formCadastro.nivelDeDificuldadeId = data.NivelDeDificuldade.Id;
            //            $scope.formCadastro.ativo = data.Ativo;
            //            $scope.formCadastro.exerciciosTreinoA = data.ExerciciosTreinoA;
            //            $scope.formCadastro.exerciciosTreinoB = data.ExerciciosTreinoB;
            //            $scope.formCadastro.exerciciosTreinoC = data.ExerciciosTreinoC;
            //            $scope.formCadastro.exerciciosTreinoD = data.ExerciciosTreinoD;
            //            $scope.formCadastro.exerciciosTreinoE = data.ExerciciosTreinoE;
            //            $scope.formCadastro.exerciciosTreinoF = data.ExerciciosTreinoF;
            //        }

            //        $scope.carregando = false;
            //        $scope.exibirFormEditar = true;
            //        $scope.exibirBotaoSalvar = true;
            //    })
            //    .error(function (error) {
            //        if (error != null) $scope.status = 'Erro ao carregar';
            //        $scope.carregando = false;
            //    });
        };

        $scope.ativar = function (index) {

            $scope.carregando = true;

            var method = 'POST';
            var url = configApp.serverUrl + '/api/treino/ativar';
            $scope.treinos[index].Ativo = !$scope.treinos[index].Ativo;

            baseService.invoke(method, url, $scope.treinos[index])
                 .success(function (data) {

                     if ($scope.treinos[index].Ativo) {
                         localStorageService.remover('treino');
                         localStorageService.salvar('treino', $scope.treinos[index]);
                     } else {
                         localStorageService.remover('treino');
                     }

                     $scope.treinos = data;

                     localStorageService.remover('treinos');
                     localStorageService.salvar('treinos', $scope.treinos);

                     $scope.carregando = false;
                     $scope.temRegistro = true;
                 })
                 .error(function (erro) {
                     $scope.carregando = false;
                 });
        };

        $scope.ativo = function () {
            $scope.formCadastro.ativo = !$scope.formCadastro.ativo;
        };

        $scope.voltarParaAlunos = function () {
            $scope.subMenuEsconder();
            $location.url('/alunos');
        }

        //----- Adicionar Exercicio Inicio

        $scope.abrirAdicionarExercicio = function () {
            $scope.exibirFormAdicionarExercicio = true;
            $scope.formCadastro.adicionarExercicioMensagem = '';
        };

        $scope.cancelarAdicionarExercicio = function () {
            $scope.exibirFormAdicionarExercicio = false;
            $scope.formCadastro.adicionarExercicioMensagem = '';
        }

        $scope.adicionarExercicioAE = function () {

            $scope.carregando = true;

            var method = 'POST';
            var url = configApp.serverUrl + '/api/Exercicio/salvar';

            var exercicio = {
                GrupoMuscularId: $scope.formCadastro.grupoMuscularAE.Id,
                Descricao: $scope.formCadastro.exercicioAE
            };

            baseService.invoke(method, url, exercicio)
                 .success(function (data) {
                     switch (data[0].GrupoMuscularId) {
                         case 1:
                             localStorageService.salvar("ExerciciosDePeito", data);
                             break;

                         case 2:
                             localStorageService.salvar("ExerciciosDeCostas", data);
                             break;

                         case 3:
                             localStorageService.salvar("ExerciciosDePerna", data);
                             break;

                         case 4:
                             localStorageService.salvar("ExerciciosDeOmbro", data);
                             break;

                         case 5:
                             localStorageService.salvar("ExerciciosDeBiceps", data);
                             break;

                         case 6:
                             localStorageService.salvar("ExerciciosDeTriceps", data);
                             break;

                         case 7:
                             localStorageService.salvar("ExerciciosDeAbdominal", data);
                             break;

                         case 8:
                             localStorageService.salvar("ExerciciosDeAerobico", data);
                             break;
                     }

                     preencheComboExercicios(data);

                     $scope.formCadastro.adicionarExercicioMensagem = 'Exercício adicionado com sucesso';
                     $scope.formCadastro.exercicioAE = '';
                     $scope.carregando = false;
                 })
                 .error(function (erro) {
                     $scope.formCadastro.adicionarExercicioMensagem = 'Erro ao adicionar exercício';
                     $scope.carregando = false;
                 });
        }

        //-----Adicionar Exercicio Fim

        try {
            init();
        } catch (e) {
            alert(e.message);
        }
    }
]);