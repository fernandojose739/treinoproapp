﻿'use strict';
marombaApp.controller('alunosController', [
    '$routeParams', '$rootScope', '$scope', 'baseService', 'configApp', '$location', 'sessionService',
    function ($routeParams, $rootScope, $scope, baseService, configApp, $location, sessionService) {

        function init() {
            sessionService.validarSession();

            $scope.status = '';
            $scope.nome = '';
            $scope.alertaMensagem = '';
            $scope.temRegistro = false;
            $scope.carregando = false;
            $scope.alerta = false;
            $scope.menuExibir = false;
            $scope.verAnamnese = false;
            $scope.verTreinos = false;
            $scope.adicionarTreinoExibir = false;
            $scope.alunos = {};
            $scope.treino = {};
            $scope.ordenarPor = 'nome';
            $scope.ordenarPorAtivoDesc = 'Ativo';

            $scope.listar();
        }

        $scope.listar = function () {

            $scope.carregando = true;

            $scope.menuExibir = false;
            $scope.alerta = false;

            var alunoNome = $scope.nome;
            if ($scope.nome == '')
                alunoNome = 'TodosAlunos';

            var method = 'GET';
            var url = configApp.serverUrl + '/api/alunos/listar/' + $rootScope.usuario.Id + '/' + alunoNome;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.alunos = data;
                        $scope.temRegistro = true;
                        $scope.ordenarPor = 'nome';
                    } else {
                        $scope.temRegistro = false;
                    }

                    $scope.carregando = false;
                })
                .error(function (error) {
                    $scope.temRegistro = false;
                    $scope.carregando = false;
                });
        };

        $scope.exibirSubMenu = function (index) {
            $scope.alunos[index].SubMenuExibir = !$scope.alunos[index].SubMenuExibir;
        };

        $scope.anamnese = function (index) {
            var aluno = $scope.alunos[index];

            $scope.alerta = false;
            $scope.menuExibir = false;

            if (aluno.Anamneses.length <= 0) {
                $scope.alerta = true;
                $scope.alertaMensagem = aluno.Nome + ' não preencheu anamnese';
                return;
            }

            $scope.adicionarTreinoExibir = false;
            $scope.menuExibir = true;
            $scope.verAnamnese = true;
            $scope.Anamnese = {};
            $scope.Anamnese.AlunoNome = aluno.Nome;
            $scope.Anamnese.DataDeNascimento = aluno.Anamneses[0].DataDeNascimento;
            $scope.Anamnese.AtividadesDaVidaDiaria = aluno.Anamneses[0].AtividadesDaVidaDiaria;
            $scope.Anamnese.HistoricoMedico = aluno.Anamneses[0].HistoricoMedico;
            $scope.Anamnese.Alergia = aluno.Anamneses[0].Alergia;
            $scope.Anamnese.Acidente = aluno.Anamneses[0].Acidente;
            $scope.Anamnese.RestricaoAtividadeFisica = aluno.Anamneses[0].RestricaoAtividadeFisica;
            $scope.Anamnese.Fumante = aluno.Anamneses[0].Fumante;
            $scope.Anamnese.RealizaAtividadeFisica = aluno.Anamneses[0].RealizaAtividadeFisica;
            $scope.Anamnese.DiasParaTreinar = aluno.Anamneses[0].DiasParaTreinar;
            $scope.Anamnese.HorasParaTreinarPorDia = aluno.Anamneses[0].HorasParaTreinarPorDia;
            $scope.Anamnese.ComentariosGerais = aluno.Anamneses[0].ComentariosGerais;
        };

        $scope.treinosPorAluno = function (index) {

            $scope.carregando = true;

            var aluno = $scope.alunos[index];

            var method = 'GET';
            var url = configApp.serverUrl + '/api/treino/listar/' + aluno.Id;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.adicionarTreinoExibir = true;
                        $scope.menuExibir = true;
                        $scope.verTreinos = true;

                        $scope.treino = {};
                        $scope.treino.AlunoId = aluno.Id;
                        $scope.treino.AlunoNome = aluno.Nome;
                        $scope.treino.treinos = data;
                    } else {
                        $scope.alerta = true;
                        $scope.alertaMensagem = 'Não foi encontrado treinos para ' + aluno.Nome;
                    }

                    $scope.carregando = false;
                })
                .error(function (error) {
                    $scope.temRegistro = false;
                    $scope.carregando = false;
                });
        }

        $scope.subMenuExibir = function () {
            $scope.submenu = !$scope.submenu;
        };

        $scope.subMenuEsconder = function () {
            $scope.submenu = false;
        };

        $scope.voltarParaLista = function () {
            $scope.subMenuExibir();
            $scope.verAnamnese = false;
            $scope.verTreinos = false;
            $scope.menuExibir = false;
        };

        $scope.adicionarTreino = function (alunoId) {

            var treinoId = -1;
            var professorId = $rootScope.usuario.Id;
            var alunoNome = $scope.treino.AlunoNome;

            $location.url('/treino/' + alunoNome + '/' + alunoId + '/' + professorId + '/' + treinoId);
        };

        $scope.editarTreino = function (index) {

            var treino = $scope.treino.treinos[index];
            var treinoId = treino.Id;
            var alunoId = treino.UsuarioId;
            var alunoNome = $scope.treino.AlunoNome;
            var professorId = $rootScope.usuario.Id;

            $location.url('/treino/' + alunoNome + '/' + alunoId + '/' + professorId + '/' + treinoId);
        };

        init();
    }
]);