﻿'use strict';
marombaApp.controller('professorController', [
    '$routeParams', '$rootScope', '$scope', 'professorService', 'sessionService',
    function ($routeParams, $rootScope, $scope, professorService, sessionService) {

        sessionService.validarSession();

        $rootScope.exibirBotaoNovo = false;
        $scope.labelBuscar = "Buscar";
        $scope.status = '';
        $scope.nome = '';
        $scope.ordenarPor = 'Nome';
        $scope.temRegistro = false;
        $scope.professores = {};
        $scope.carregando = false;

        $scope.listar = function () {

            $scope.carregando = true;

            professorService.listar($rootScope.usuario.Id, $scope.nome)
                .success(function (data) {
                    $scope.professores = data;
                    $scope.ordenarPor = 'Nome';
                    $scope.temRegistro = true;
                    $scope.carregando = false;
                })
                .error(function (error) {

                    if (error != null)
                        $scope.status = 'Erro ao carregar: ' + error.message;

                    $scope.temRegistro = false;
                    //$scope.labelBuscar = "Buscar";
                    $scope.carregando = false;
                });
        };

        $scope.selecionar = function(professorId) {

            $scope.carregando = true;

            var usuarioProfessor = {};
            usuarioProfessor.UsuarioId = $rootScope.usuario.Id;
            usuarioProfessor.ProfessorId = professorId;

            professorService.salvarUsuarioProfessor(usuarioProfessor)
                .success(function (data) {
                    $scope.listar();
                })
                .error(function(error) {
                    $scope.carregando = false;
                });
        };

        $scope.remover = function (professorId) {

            $scope.carregando = true;

            var usuarioProfessor = {};
            usuarioProfessor.UsuarioId = $rootScope.usuario.Id;
            usuarioProfessor.ProfessorId = professorId;

            professorService.removerUsuarioProfessor(usuarioProfessor)
                .success(function (data) {
                    $scope.listar();
                })
                .error(function (error) {
                    $scope.carregando = false;
                });
        };
    }
]);