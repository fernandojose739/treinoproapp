﻿'use strict';
marombaApp.controller('alunosTreinoController', [
    '$routeParams', 'baseService', '$rootScope', '$scope', 'configApp', 'localStorageService', '$location', 'sessionService',
    function ($routeParams, baseService, $rootScope, $scope, configApp, localStorageService, $location, sessionService) {

        sessionService.validarSession();

        $scope.subMenuExibir = function () {
            $scope.submenu = !$scope.submenu;
        };

        $scope.subMenuEsconder = function () {
            $scope.submenu = false;
        };

        function init() {

            $scope.carregando = true;
            initVarGerais();
            carregarForm();
        };

        function carregarForm() {
            var treinoId = $routeParams.treinoId;

            initFormEditar();
            if (treinoId != undefined && treinoId != '' && treinoId > 0) {
                $scope.obter(treinoId);
            } else {
                $scope.carregando = false;
            }
        }

        function initVarGerais() {
            $scope.submenu = false;
            $scope.status = "";
        }

        function initFormEditar() {
            $scope.nivelDeDificuldades = {};
            $scope.dias = {};
            $scope.objetivoTreinos = {};
            $scope.exibirBotaoSalvar = true;

            $scope.formCadastro = {};
            $scope.formCadastro.ativo = false;
            $scope.formCadastro.usuarioNome = $routeParams.alunoNome;
            $scope.formCadastro.usuarioId = $routeParams.alunoId;
            $scope.formCadastro.usuarioIdCriadoPor = $routeParams.professorId;
            $scope.formCadastro.mensagemAdicionarExercicio = "";
            $scope.formCadastro.nome = "";
            $scope.formCadastro.objetivoTreino = "";
            $scope.formCadastro.nivelDeDificuldade = "";
            $scope.formCadastro.dia = {};

            $scope.formCadastro.dataDeCriacao = new Date();
            $scope.formCadastro.dataDeAlteracao = new Date();

            $scope.formCadastro.grupoMuscularesA = {};
            $scope.formCadastro.grupoMuscularesB = {};
            $scope.formCadastro.grupoMuscularesC = {};
            $scope.formCadastro.grupoMuscularesD = {};
            $scope.formCadastro.grupoMuscularesE = {};
            $scope.formCadastro.grupoMuscularesF = {};
            $scope.formCadastro.grupoMuscularA = "";
            $scope.formCadastro.grupoMuscularB = "";
            $scope.formCadastro.grupoMuscularC = "";
            $scope.formCadastro.grupoMuscularD = "";
            $scope.formCadastro.grupoMuscularE = "";
            $scope.formCadastro.grupoMuscularF = "";

            initComboExercicios();

            limpaCampos('A');
            limpaCampos('B');
            limpaCampos('C');
            limpaCampos('D');
            limpaCampos('E');
            limpaCampos('F');

            $scope.formCadastro.exerciciosTreinoA = [];
            $scope.formCadastro.exerciciosTreinoB = [];
            $scope.formCadastro.exerciciosTreinoC = [];
            $scope.formCadastro.exerciciosTreinoD = [];
            $scope.formCadastro.exerciciosTreinoE = [];
            $scope.formCadastro.exerciciosTreinoF = [];

            $scope.treinoA = false;
            $scope.treinoB = false;
            $scope.treinoC = false;
            $scope.treinoD = false;
            $scope.treinoE = false;
            $scope.treinoF = false;

            //--- Carregar combos
            listarNivelDeDificuldade();
            listarObjetivoTreino();
            listarDias();
            listarGrupoMuscular();
        };

        function listarDias() {
            var method = 'GET';
            var url = configApp.serverUrl + '/api/dia/listar';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.dias = data;
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                });
        };

        function listarNivelDeDificuldade() {
            var method = 'GET';
            var url = configApp.serverUrl + '/api/treino/listarNivelDeDificuldade';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.nivelDeDificuldades = data;
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                });
        };

        function listarGrupoMuscular() {

            var method = 'GET';
            var url = configApp.serverUrl + '/api/GrupoMuscular/listar';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.formCadastro.grupoMuscularesaA = data;
                        $scope.formCadastro.grupoMuscularesaB = data;
                        $scope.formCadastro.grupoMuscularesaC = data;
                        $scope.formCadastro.grupoMuscularesaD = data;
                        $scope.formCadastro.grupoMuscularesaE = data;
                        $scope.formCadastro.grupoMuscularesaF = data;
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                });
        };

        function listarObjetivoTreino() {

            var method = 'GET';
            var url = configApp.serverUrl + '/api/ObjetivoTreino/listar';

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.objetivoTreinos = data;
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                });
        };

        $scope.listarExerciciosPorGrupoMuscular = function () {

            $scope.carregando = true;

            initComboExercicios();

            var dia = $scope.formCadastro.dia.Descricao;
            var grupoMuscularId = $scope.formCadastro.grupoMuscularA.Id;

            switch (dia) {
                case "B":
                    grupoMuscularId = $scope.formCadastro.grupoMuscularB.Id;
                    break;

                case "C":
                    grupoMuscularId = $scope.formCadastro.grupoMuscularC.Id;
                    break;

                case "D":
                    grupoMuscularId = $scope.formCadastro.grupoMuscularD.Id;
                    break;

                case "E":
                    grupoMuscularId = $scope.formCadastro.grupoMuscularE.Id;
                    break;

                case "F":
                    grupoMuscularId = $scope.formCadastro.grupoMuscularF.Id;
                    break;
            }

            var method = 'GET';
            var url = configApp.serverUrl + '/api/exercicio/listarPorGrupoMuscular/' + grupoMuscularId;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        preencheComboExercicios(data);
                        $scope.carregando = false;
                    }
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                });
        };

        function initComboExercicios() {
            $scope.formCadastro.exerciciosA = {};
            $scope.formCadastro.exerciciosB = {};
            $scope.formCadastro.exerciciosC = {};
            $scope.formCadastro.exerciciosD = {};
            $scope.formCadastro.exerciciosE = {};
            $scope.formCadastro.exerciciosF = {};
        };

        function preencheComboExercicios(data) {
            switch ($scope.formCadastro.dia.Descricao) {
                case "A":
                    $scope.formCadastro.exerciciosA = data;
                    break;

                case "B":
                    $scope.formCadastro.exerciciosB = data;
                    break;

                case "C":
                    $scope.formCadastro.exerciciosC = data;
                    break;

                case "D":
                    $scope.formCadastro.exerciciosD = data;
                    break;

                case "E":
                    $scope.formCadastro.exerciciosE = data;
                    break;

                case "F":
                    $scope.formCadastro.exerciciosF = data;
                    break;
            }
        }

        $scope.alterarDia = function () {

            var aba = $scope.formCadastro.dia.Descricao;

            $scope.treinoA = false;
            $scope.treinoB = false;
            $scope.treinoC = false;
            $scope.treinoD = false;
            $scope.treinoE = false;
            $scope.treinoF = false;
            if (aba == 'A') {
                $scope.treinoA = true;
                return;
            }

            if (aba == 'B') {
                $scope.treinoB = true;
                return;
            }

            if (aba == 'C') {
                $scope.treinoC = true;
                return;
            }

            if (aba == 'D') {
                $scope.treinoD = true;
                return;
            }

            if (aba == 'E') {
                $scope.treinoE = true;
                return;
            }

            if (aba == 'F') {
                $scope.treinoF = true;
                return;
            }
        }

        $scope.voltarParaAlunos = function () {
            $scope.subMenuEsconder();
            $location.url('/alunos');
        }

        $scope.salvar = function () {

            $scope.carregando = true;

            $scope.status = '';

            if (!validaCamposParaSalvar()) {
                $scope.carregando = false;
                return;
            }

            var method = 'POST';
            var url = configApp.serverUrl + '/api/treino/salvar';

            baseService.invoke(method, url, $scope.formCadastro)
                 .success(function (response) {
                     $scope.exibirBotaoSalvar = false;
                     if (response.Erros.length == 0) {
                         $scope.status = 'Sucesso ao salvar o treino';

                         if (response.Ativo) {
                             localStorageService.salvar('treino', response);
                         }
                     } else {
                         $scope.status = 'Erro ao salvar o treino';
                     }

                     if (window.confirm($scope.status + '. Deseja voltar para alunos?')) {
                         $location.url('/alunos');
                     }

                     $scope.carregando = false;
                 })
                 .error(function (erro) {
                     $scope.exibirBotaoSalvar = false;
                     $scope.carregando = false;
                     $scope.temRegistro = false;
                     $scope.status = 'Erro ao salvar formulário';

                     if (window.confirm($scope.status + '. Deseja voltar para alunos?')) {
                         $location.url('/alunos');
                     }
                 });
        };

        function validaCamposParaSalvar() {
            if ($scope.formCadastro.nome == '') {
                $scope.status = 'Nome é obrigatório';
                return false;
            }

            if ($scope.formCadastro.objetivoTreinoId == 0) {
                $scope.status = 'Objetivo é obrigatório';
                return false;
            }

            if ($scope.formCadastro.nivelDeDificuldadeId == 0) {
                $scope.status = 'Nível de dificuldade é obrigatório';
                return false;
            }

            return true;
        }

        $scope.removerExercicioDeTreino = function (index) {

            var dia = $scope.formCadastro.dia.Descricao;

            if (dia == "A") {
                $scope.formCadastro.exerciciosTreinoA.splice(index, 1);
                return;
            }

            if (dia == "B") {
                $scope.formCadastro.exerciciosTreinoB.splice(index, 1);
                return;
            }

            if (dia == "C") {
                $scope.formCadastro.exerciciosTreinoC.splice(index, 1);
                return;
            }

            if (dia == "D") {
                $scope.formCadastro.exerciciosTreinoD.splice(index, 1);
                return;
            }

            if (dia == "E") {
                $scope.formCadastro.exerciciosTreinoE.splice(index, 1);
                return;
            }

            if (dia == "F") {
                $scope.formCadastro.exerciciosTreinoF.splice(index, 1);
                return;
            }
        };

        $scope.adicionarExercicio = function () {

            $scope.status = '';

            var dia = $scope.formCadastro.dia.Descricao;
            $scope.formCadastro.mensagemAdicionarExercicio = "";

            $scope.carregando = true;

            if (!validaCamposParaAdicionarExercicio()) {
                $scope.formCadastro.mensagemAdicionarExercicio = "Todos os campos são obrigátorio";
                $scope.carregando = false;
                return;
            }

            if (dia == "A") {

                var exerciciosTreinoA = {};
                exerciciosTreinoA.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoA.Exercicio = {};
                exerciciosTreinoA.Exercicio.Descricao = $scope.formCadastro.exercicioA.Descricao;
                exerciciosTreinoA.Exercicio.Id = $scope.formCadastro.exercicioA.Id;
                exerciciosTreinoA.ExercicioId = $scope.formCadastro.exercicioA.Id;
                exerciciosTreinoA.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesA;
                exerciciosTreinoA.Repeticoes = $scope.formCadastro.repeticoesA;
                exerciciosTreinoA.Carga = $scope.formCadastro.cargaA;
                exerciciosTreinoA.Ordem = $scope.formCadastro.ordemA;
                exerciciosTreinoA.TempoDeDescando = $scope.formCadastro.tempoDeDescandoA;

                $scope.formCadastro.exerciciosTreinoA.push(exerciciosTreinoA);

                limpaCampos('A');
                $scope.carregando = false;
                return;
            }

            if (dia == "B") {
                var exerciciosTreinoB = {};
                exerciciosTreinoB.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoB.Exercicio = {};
                exerciciosTreinoB.Exercicio.Descricao = $scope.formCadastro.exercicioB.Descricao;
                exerciciosTreinoB.Exercicio.Id = $scope.formCadastro.exercicioB.Id;
                exerciciosTreinoB.ExercicioId = $scope.formCadastro.exercicioB.Id;
                exerciciosTreinoB.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesB;
                exerciciosTreinoB.Repeticoes = $scope.formCadastro.repeticoesB;
                exerciciosTreinoB.Carga = $scope.formCadastro.cargaB;
                exerciciosTreinoB.Ordem = $scope.formCadastro.ordemB;
                exerciciosTreinoB.TempoDeDescando = $scope.formCadastro.tempoDeDescandoB;

                $scope.formCadastro.exerciciosTreinoB.push(exerciciosTreinoB);

                limpaCampos('B');
                $scope.carregando = false;
            }

            if (dia == "C") {

                var exerciciosTreinoC = {};
                exerciciosTreinoC.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoC.Exercicio = {};
                exerciciosTreinoC.Exercicio.Descricao = $scope.formCadastro.exercicioC.Descricao;
                exerciciosTreinoC.Exercicio.Id = $scope.formCadastro.exercicioC.Id;
                exerciciosTreinoC.ExercicioId = $scope.formCadastro.exercicioC.Id;
                exerciciosTreinoC.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesC;
                exerciciosTreinoC.Repeticoes = $scope.formCadastro.repeticoesC;
                exerciciosTreinoC.Carga = $scope.formCadastro.cargaC;
                exerciciosTreinoC.Ordem = $scope.formCadastro.ordemC;
                exerciciosTreinoC.TempoDeDescando = $scope.formCadastro.tempoDeDescandoC;

                $scope.formCadastro.exerciciosTreinoC.push(exerciciosTreinoC);

                limpaCampos('C');
                $scope.carregando = false;
                return;
            }

            if (dia == "D") {

                var exerciciosTreinoD = {};
                exerciciosTreinoD.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoD.Exercicio = {};
                exerciciosTreinoD.Exercicio.Descricao = $scope.formCadastro.exercicioD.Descricao;
                exerciciosTreinoD.Exercicio.Id = $scope.formCadastro.exercicioD.Id;
                exerciciosTreinoD.ExercicioId = $scope.formCadastro.exercicioD.Id;
                exerciciosTreinoD.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesD;
                exerciciosTreinoD.Repeticoes = $scope.formCadastro.repeticoesD;
                exerciciosTreinoD.Carga = $scope.formCadastro.cargaD;
                exerciciosTreinoD.Ordem = $scope.formCadastro.ordemD;
                exerciciosTreinoD.TempoDeDescando = $scope.formCadastro.tempoDeDescandoD;

                $scope.formCadastro.exerciciosTreinoD.push(exerciciosTreinoD);

                limpaCampos('D');
                $scope.carregando = false;
                return;
            }

            if (dia == "E") {

                var exerciciosTreinoE = {};
                exerciciosTreinoE.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoE.Exercicio = {};
                exerciciosTreinoE.Exercicio.Descricao = $scope.formCadastro.exercicioE.Descricao;
                exerciciosTreinoE.Exercicio.Id = $scope.formCadastro.exercicioE.Id;
                exerciciosTreinoE.ExercicioId = $scope.formCadastro.exercicioE.Id;
                exerciciosTreinoE.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesE;
                exerciciosTreinoE.Repeticoes = $scope.formCadastro.repeticoesE;
                exerciciosTreinoE.Carga = $scope.formCadastro.cargaE;
                exerciciosTreinoE.Ordem = $scope.formCadastro.ordemE;
                exerciciosTreinoE.TempoDeDescando = $scope.formCadastro.tempoDeDescandoE;

                $scope.formCadastro.exerciciosTreinoE.push(exerciciosTreinoE);

                limpaCampos('E');
                $scope.carregando = false;
                return;
            }

            if (dia == "F") {

                var exerciciosTreinoF = {};
                exerciciosTreinoF.DiaId = $scope.formCadastro.dia.Id;
                exerciciosTreinoF.Exercicio = {};
                exerciciosTreinoF.Exercicio.Descricao = $scope.formCadastro.exercicioF.Descricao;
                exerciciosTreinoF.Exercicio.Id = $scope.formCadastro.exercicioF.Id;
                exerciciosTreinoF.ExercicioId = $scope.formCadastro.exercicioF.Id;
                exerciciosTreinoF.QuantidadeDeSeries = $scope.formCadastro.quantidadeDeSeriesF;
                exerciciosTreinoF.Repeticoes = $scope.formCadastro.repeticoesF;
                exerciciosTreinoF.Carga = $scope.formCadastro.cargaF;
                exerciciosTreinoF.Ordem = $scope.formCadastro.ordemF;
                exerciciosTreinoF.TempoDeDescando = $scope.formCadastro.tempoDeDescandoF;

                $scope.formCadastro.exerciciosTreinoF.push(exerciciosTreinoF);

                limpaCampos('F');
                $scope.carregando = false;
                return;
            }
        };

        function validaCamposParaAdicionarExercicio() {

            var dia = $scope.formCadastro.dia.Descricao;

            if ((dia == 'A') && ($scope.formCadastro.exercicioA == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesA == "" ||
                                 $scope.formCadastro.repeticoesA == "" ||
                                 $scope.formCadastro.cargaA == "" ||
                                 $scope.formCadastro.ordemA == "" ||
                                 $scope.formCadastro.tempoDeDescandoA == "")) {
                return false;
            }

            if ((dia == 'B') && ($scope.formCadastro.exercicioB == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesB == "" ||
                                 $scope.formCadastro.repeticoesB == "" ||
                                 $scope.formCadastro.cargaB == "" ||
                                 $scope.formCadastro.ordemB == "" ||
                                 $scope.formCadastro.tempoDeDescandoB == "")) {
                return false;
            }

            if ((dia == 'C') && ($scope.formCadastro.exercicioC == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesC == "" ||
                                 $scope.formCadastro.repeticoesC == "" ||
                                 $scope.formCadastro.cargaC == "" ||
                                 $scope.formCadastro.ordemC == "" ||
                                 $scope.formCadastro.tempoDeDescandoC == "")) {
                return false;
            }

            if ((dia == 'D') && ($scope.formCadastro.exercicioD == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesD == "" ||
                                 $scope.formCadastro.repeticoesD == "" ||
                                 $scope.formCadastro.cargaD == "" ||
                                 $scope.formCadastro.ordemD == "" ||
                                 $scope.formCadastro.tempoDeDescandoD == "")) {
                return false;
            }

            if ((dia == 'E') && ($scope.formCadastro.exercicioE == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesE == "" ||
                                 $scope.formCadastro.repeticoesE == "" ||
                                 $scope.formCadastro.cargaE == "" ||
                                 $scope.formCadastro.ordemE == "" ||
                                 $scope.formCadastro.tempoDeDescandoE == "")) {
                return false;
            }

            if ((dia == 'F') && ($scope.formCadastro.exercicioF == "" ||
                                 $scope.formCadastro.quantidadeDeSeriesF == "" ||
                                 $scope.formCadastro.repeticoesF == "" ||
                                 $scope.formCadastro.cargaF == "" ||
                                 $scope.formCadastro.ordemF == "" ||
                                 $scope.formCadastro.tempoDeDescandoF == "")) {
                return false;
            }

            return true;
        }

        function limpaCampos(dia) {

            if (dia == 'A') {
                $scope.formCadastro.exercicioA = "";
                $scope.formCadastro.ordemA = parseFloat($scope.formCadastro.ordemA) + 1;
                $scope.formCadastro.tempoDeDescandoA = "";
                return;
            }

            if (dia == 'B') {
                $scope.formCadastro.exercicioB = "";
                $scope.formCadastro.ordemB = parseFloat($scope.formCadastro.ordemB) + 1;
                $scope.formCadastro.tempoDeDescandoB = "";
            }

            if (dia == 'C') {
                $scope.formCadastro.exercicioC = "";
                $scope.formCadastro.ordemC = parseFloat($scope.formCadastro.ordemC) + 1;
                $scope.formCadastro.tempoDeDescandoC = "";
            }

            if (dia == 'D') {
                $scope.formCadastro.exercicioD = "";
                $scope.formCadastro.ordemD = parseFloat($scope.formCadastro.ordemD) + 1;
                $scope.formCadastro.tempoDeDescandoD = "";
            }

            if (dia == 'E') {
                $scope.formCadastro.exercicioE = "";
                $scope.formCadastro.ordemE = parseFloat($scope.formCadastro.ordemE) + 1;
                $scope.formCadastro.tempoDeDescandoE = "";
            }

            if (dia == 'F') {
                $scope.formCadastro.exercicioF = "";
                $scope.formCadastro.ordemF = parseFloat($scope.formCadastro.ordemF) + 1;
                $scope.formCadastro.tempoDeDescandoF = "";
            }
        }

        $scope.obter = function (id) {

            var method = 'GET';
            var url = configApp.serverUrl + '/api/treino/obter/' + id;

            baseService.invoke(method, url, "")
                .success(function (data) {

                    initFormEditar();
                    if (data != null) {

                        $scope.formCadastro.id = data.Id;
                        $scope.formCadastro.dataDeCriacao = data.DataDeCriacao;
                        $scope.formCadastro.dataDeAlteracao = new Date();
                        $scope.formCadastro.nome = data.Nome;
                        $scope.formCadastro.objetivoTreinoId = data.ObjetivoTreino.Id;
                        $scope.formCadastro.nivelDeDificuldadeId = data.NivelDeDificuldade.Id;
                        $scope.formCadastro.ativo = data.Ativo;
                        $scope.formCadastro.exerciciosTreinoA = data.ExerciciosTreinoA;
                        $scope.formCadastro.exerciciosTreinoB = data.ExerciciosTreinoB;
                        $scope.formCadastro.exerciciosTreinoC = data.ExerciciosTreinoC;
                        $scope.formCadastro.exerciciosTreinoD = data.ExerciciosTreinoD;
                        $scope.formCadastro.exerciciosTreinoE = data.ExerciciosTreinoE;
                        $scope.formCadastro.exerciciosTreinoF = data.ExerciciosTreinoF;
                    }

                    $scope.carregando = false;
                    $scope.exibirBotaoSalvar = true;
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                });
        };

        $scope.ativar = function () {
            $scope.formCadastro.ativo = !$scope.formCadastro.ativo;
        };

        init();
    }
]);