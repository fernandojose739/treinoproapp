﻿'use strict';
marombaApp.controller('medidasController', [
    '$routeParams', '$rootScope', '$scope', 'baseService', 'configApp', 'sessionService', 'localStorageService',
    function ($routeParams, $rootScope, $scope, baseService, configApp, sessionService, localStorageService) {

        sessionService.validarSession();

        inicializaVarUteis();
        inicializaFormPesquisar();
        inicializaFormEditar();
        init();

        function listar(temQueListar) {

            //--- cache
            if (!temQueListar) {
                var medidas = localStorageService.obter('medidas');
                if (medidas != undefined) {
                    $scope.medidas = medidas;
                    $scope.temRegistro = true;
                    return;
                }
            }

            //--- db
            $scope.carregando = true;

            var method = 'GET';
            var url = configApp.serverUrl + '/api/medidas/listar/' + $scope.formPesquisar.usuarioId;

            baseService.invoke(method, url, "")
                .success(function (data) {
                    if (data.length > 0) {
                        $scope.temRegistro = true;
                        $scope.medidas = data;
                        localStorageService.salvar('medidas', data);
                    } else {
                        $scope.temRegistro = false;
                    }

                    $scope.carregando = false;
                    $scope.exibirFormEditar = false;
                    $scope.exibirBotaoSalvar = false;
                })
                .error(function (error) {
                    if (error != null) $scope.status = 'Erro ao carregar';
                    $scope.carregando = false;
                });
        };

        //$scope.adicionarEditar = function (id) {

        //    $scope.carregando = true;
        //    $scope.submenu = false;

        //    if (id > 0) {
        //        $scope.obter(id);
        //    } else {
        //        inicializaFormEditar();
        //        $scope.carregando = false;
        //    }
        //};

        $scope.obter = function (index) {

            $scope.carregando = true;

            //--- novo
            if (index == null) {
                inicializaFormEditar();
                $scope.carregando = false;
                $scope.exibirFormEditar = true;
                $scope.exibirBotaoSalvar = true;
                $scope.submenu = false;
                return;
            }

            //--- cache
            var medida = index;
            $scope.formEditar.id = medida.Id;
            $scope.formEditar.dataDeCriacao = medida.DataDeCriacao;
            $scope.formEditar.altura = medida.Altura;
            $scope.formEditar.peso = medida.Peso;
            $scope.formEditar.braco = medida.Braco;
            $scope.formEditar.perna = medida.Perna;
            $scope.formEditar.torax = medida.Torax;
            $scope.formEditar.cintura = medida.Cintura;
            $scope.formEditar.usuarioId = $rootScope.usuario.Id;
            $scope.carregando = false;
            $scope.exibirFormEditar = true;
            $scope.exibirBotaoSalvar = true;
            $scope.submenu = false;
            return;


            //$scope.submenu = false;

            //var method = 'GET';
            //var url = configApp.serverUrl + '/api/medidas/obter/' + id;

            //baseService.invoke(method, url, "")
            //    .success(function (data) {
            //        if (data != null) {
            //            $scope.formEditar.id = data.Id;
            //            $scope.formEditar.dataDeCriacao = data.DataDeCriacao;
            //            $scope.formEditar.altura = data.Altura;
            //            $scope.formEditar.peso = data.Peso;
            //            $scope.formEditar.braco = data.Braco;
            //            $scope.formEditar.perna = data.Perna;
            //            $scope.formEditar.torax = data.Torax;
            //            $scope.formEditar.cintura = data.Cintura;
            //            $scope.formEditar.usuarioId = $rootScope.usuario.Id;
            //        } else {
            //            inicializaFormEditar();
            //        }

            //        $scope.carregando = false;
            //        $scope.exibirFormEditar = true;
            //        $scope.exibirBotaoSalvar = true;
            //    })
            //    .error(function (error) {
            //        if (error != null) $scope.status = 'Erro ao carregar';
            //        $scope.carregando = false;
            //    });
        };

        $scope.salvar = function () {

            $scope.carregando = true;

            var method = 'POST';
            var url = configApp.serverUrl + '/api/medidas/salvar';

            baseService.invoke(method, url, $scope.formEditar)
                 .success(function (response) {
                     if (response.Erros.length == 0) {
                         $scope.status = 'Formulário salvo com sucesso';
                         listar(true);
                     } else {
                         $scope.status = response.Erros[0].Mensagem;
                         $scope.carregando = false;
                     }
                 })
                 .error(function () {
                     $scope.carregando = false;
                     $scope.temRegistro = false;
                     $scope.status = 'Erro ao salvar formulário';
                 });
        };

        $scope.excluir = function () {

            $scope.carregando = true;

            var method = 'POST';
            var url = configApp.serverUrl + '/api/medidas/excluir';

            baseService.invoke(method, url, $scope.formEditar)
                 .success(function (response) {
                     if (response[0].Erros.length == 0) {
                         $scope.status = 'Item excluido com sucesso';
                         listar(true);
                     } else {
                         $scope.status = response[0].Erros[0].Mensagem;
                         $scope.carregando = false;
                     }
                 })
                 .error(function () {
                     $scope.carregando = false;
                     $scope.temRegistro = false;
                     $scope.status = 'Erro ao excluir item';
                 });
        };

        $scope.voltarParaLista = function () {
            $scope.exibirFormEditar = false;
            $scope.exibirBotaoSalvar = false;
            $scope.submenu = false;
        }

        function inicializaFormEditar() {
            $scope.formEditar = {};
            $scope.formEditar.id = '';
            $scope.formEditar.datadecriacao = '';
            $scope.formEditar.altura = '';
            $scope.formEditar.peso = '';
            $scope.formEditar.braco = '';
            $scope.formEditar.perna = '';
            $scope.formEditar.torax = '';
            $scope.formEditar.cintura = '';
            $scope.formEditar.usuarioid = $rootScope.usuario.Id;
        };

        function inicializaFormPesquisar() {
            $scope.formPesquisar = {};
            $scope.formPesquisar.usuarioId = $rootScope.usuario.Id;
        };

        function inicializaVarUteis() {
            $scope.submenu = false;
            $scope.carregando = false;
            $scope.status = '';
            $scope.temRegistro = false;
            $scope.exibirFormEditar = false;
            $scope.exibirBotaoSalvar = false;
            $scope.ordenarPor = 'DataDeCriacao';
        };

        function init() {
            listar(false);
        }

        $scope.subMenuExibir = function () {
            $scope.submenu = !$scope.submenu;
        };
    }
]);