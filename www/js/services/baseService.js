﻿'use strict';
marombaApp.factory('baseService', ['configApp', '$http',
     function (configApp, $http) {

         var dataFactory = {};

         dataFactory.invoke = function (method, url, request) {

             var req = {
                 method: method,
                 url: url,
                 headers: { 'Content-Type': 'text/json' },
                 data: JSON.stringify(request),
             }

             return $http(req);
         };

         return dataFactory;
     }]);