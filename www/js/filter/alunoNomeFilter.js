﻿'use strict';

marombaApp.filter('alunoNomeFilter', formatAlunoNome);

function formatAlunoNome() {
    return function (value) {
        if (value == undefined)
            return '';

        if (value.length > 20)
            return value.substring(0, 20);
        else
            return value;
    }
}