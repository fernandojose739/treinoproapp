﻿marombaApp.directive('dateField', function ($filter) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelController) {
            ngModelController.$parsers.push(function (data) {
                //View -> Model
                var date = Date.parseExact(data, 'dd-MM-yyyy');
                ngModelController.$setValidity('date', date != null);
                return date;
            });
            ngModelController.$formatters.push(function (data) {
                //Model -> View
                return $filter('date')(data, "dd-MM-yyyy");
            });
        }
    }
});
